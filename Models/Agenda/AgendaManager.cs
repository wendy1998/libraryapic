﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using libraryApi.Data;
using libraryApi.Models.Books;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace libraryApi.Models.Agenda
{
    /**
     * Manages all things having to do with the agenda
     */
    public class AgendaManager
    {
        // Properties
        private static string[] Scopes { get; } = { CalendarService.Scope.Calendar };
        private static string ApplicationName { get; } = "Library API";
        private CalendarService Agenda { get; }
        
        /**
         * Constructor
         */
        public AgendaManager()
        {
            Agenda = SetCalender();
        }

        /**
         * Handles logging in to a google agenda
         * @return A UserCredential object.
         * @throws IOException If the credentials.json file cannot be found.
         */
        private static UserCredential GetCredentials()
        {
            // Load client secrets.

            using var stream =
                new FileStream($"{Program.ContentRoot}/Properties/credentials.json", FileMode.Open, FileAccess.Read);
            // The file token.json stores the user's access and refresh tokens, and is created
            // automatically when the authorization flow completes for the first time.
            var credPath = $"{Program.ContentRoot}/token.json";
            var credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                GoogleClientSecrets.Load(stream).Secrets,
                Scopes,
                "user",
                CancellationToken.None,
                new FileDataStore(credPath, true)).Result;

            return credential;
        }

        /**
         * Specifies a calendar
         * @return the calendar
         * @throws GeneralSecurityException if connecting to the calendar goes wrong
         * @throws IOException if property reading goes wrong
         */
        private static CalendarService SetCalender() 
        {
            // Build a new authorized API client service.
            return new CalendarService(new BaseClientService.Initializer
            {
                HttpClientInitializer = GetCredentials(),
                ApplicationName = AgendaManager.ApplicationName
            });
        }

        /**
         * Add a specific book to the agenda
         * @param book the book to add
         * @throws IOException if adding to the agenda goes wrong
         * @throws DatabaseException if database connectivity goes wrong
         */
        public void AddToAgenda(NotReleasedBook book) {
            // title, start and end date
            // setup title
            var summary = $" - {book.Title} uit!!!";
            if (book.Series != null && book.Series.SeriesNr != 0)
            {
                if (!string.IsNullOrEmpty(book.NumberInSeries))
                {
                    summary = $"{book.Series.Name} {book.NumberInSeries}{summary}";
                }
                else
                {
                    summary = $"{book.Series.Name}{summary}";
                }
            }
            else
            {
                summary = $"{book.Author.FullName}{summary}";
            }

            try
            {
                var release = new EventDateTime {Date = book.ReleaseDate};
                var ev = new Event
                {
                    Summary = summary,
                    Start = release,
                    End = release
                };

                // reminders
                // way of reminding is a popup 10 minutes before the actual start of the event
                EventReminder[] reminderOverrides =
                {
                    new EventReminder {Method = "popup", Minutes = 10}
                };

                // set the reminders
                var reminders = new Event.RemindersData
                {
                    UseDefault = false,
                    Overrides = reminderOverrides
                };

                ev.Reminders = reminders;

                // insert into agenda
                const string calendarId = "primary";
                ev = Agenda.Events.Insert(ev, calendarId).Execute();
                Console.Out.WriteLine($"Event created: {ev.HtmlLink}\n");
            }
            catch (Exception e)
            {
                throw new Exception($"AgendaManager: AddToAgenda - {e.GetType()}: {e.Message}");
            }

            // update database and object
            var manager = ConnectionManager.DefaultManager();
            var query = "UPDATE `boeken_die_nog_niet_uit_zijn` " +
                        $"SET `NOG_AGENDA` = '0' WHERE ID = '{book.TableId}';";
            manager.Update(query);
            book.StillAgenda = false;
        }
    }
}