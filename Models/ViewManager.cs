﻿using System;
using System.Collections.Generic;
using libraryApi.Data;
using libraryApi.Models.Books;

namespace libraryApi.Models
{
    public class ViewManager
    {
        private static List<Dictionary<string, object>> GetViewInformation(string sql)
        {
            var output = new List<Dictionary<string, object>>();
            var connManager = ConnectionManager.DefaultManager();
            var results = connManager.Select(sql);
            for (var i = 0; i < results["TITEL"].Count; i++)
            {
                var info = new Dictionary<string, object>();
                foreach (var (column, values) in results)
                {
                    info[column] = values[i];
                }
                output.Add(info);
            }
            return output;
        }

        /**
         * Used to get all the books that need to be added to the calender.
         */
        public static List<Dictionary<string, object>> GetStillAgenda()
        {
            const string sql = "SELECT * FROM `boeken_die_nog_niet_uit_zijn` where NOG_AGENDA = '1';";
            return GetViewInformation(sql);
        }

        /**
         * Used to get all the books that need to be modified in calibre.
         */
        public static List<Dictionary<string, object>> GetStillCalibre()
        {
            const string sql = "SELECT * FROM `mijn_boeken` where NOG_CALIBRE = '1';";
            return GetViewInformation(sql);
        }

        /**
         * Used to get all the books that need to be added to the ereader.
         */
        public static List<Dictionary<string, object>> GetStillEreader()
        {
            const string sql = "SELECT * FROM `mijn_boeken` where NOG_EREADER = '1';";
            return GetViewInformation(sql);
        }

        /**
         * Used to get all the books that need to be added to the ereader.
         */
        public static List<Dictionary<string, object>> GetStillBackup()
        {
            const string sql = "SELECT * FROM `mijn_boeken` where NOG_BACKUP = '1';";
            return GetViewInformation(sql);
        }
    }
}