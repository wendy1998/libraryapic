﻿#nullable enable
using static libraryApi.Data.ConnectionManager;
namespace libraryApi.Models.SeriesManagement
{
    /**
     * Represents a series
     */
    public class Series
    {
        // Properties
        public int SeriesNr { get; }
        public Author Author { get; set; }
        public string Name { get; set; }
        public bool Complete { get; set; }
        public bool TooBoring { get; set; }
        public bool Read { get; set; }
        public int Rating { get; set; }
        
        // Constructors
        
        /**
         * constructor
         * @param seriesNr: the id of the series in the database
         * @param author: the author who wrote the series
         * @param name: the name of the series
         * @param complete: whether I have all the books of the series in the database
         * @param tooBoring: whether it was too boring and I didn't finish it. default false
         * @param read: whether I read it. default false
         */
        public Series(int seriesNr, 
            Author author, 
            string name, 
            bool complete, 
            bool tooBoring=false, 
            bool read=false,
            int rating=3)
        {
            SeriesNr = seriesNr;
            Author = author;
            Name = name;
            Complete = complete;
            TooBoring = tooBoring;
            Read = read;
            Rating = rating;
        }

        /**
         * constructor
         * @param seriesNr the id of the series in the database
         * @throws DatabaseException if database connectivity goes wrong
         */
        public Series(int seriesNr)
        {
            SeriesNr = seriesNr;
            ExtractSeriesInfo();
        }
        
        // Methods
        /**
         * collect all info on the series from the database using the series id
         * @throws DatabaseException if database connectivity goes wrong
         */
        private void ExtractSeriesInfo()
        {
            // execute query
            var manager = DefaultManager();
            var sql = $"select * from series where SERIENR = {SeriesNr.ToString()}";
            var output = manager.Select(sql);
            
            // store output
            Name = output["SERIE"][0].ToString()!;
            Complete = (bool) output["COMPLEET"][0];
            TooBoring = (bool) output["TOO_BORING"][0];
            Read = (bool) output["GELEZEN"][0];
            Author = new Author(int.Parse(output["AUTEURNR"][0].ToString()));
            Rating = (int) output["WAARDERING"][0];
        }

        public override string ToString()
        {
            return $"Series with id {SeriesNr.ToString()} and name {Name} and author {Author} and rating {Rating.ToString()}.";
        }

        public override bool Equals(object? obj)
        {
            var series = (Series) obj!;
            return series != null && series.SeriesNr == SeriesNr;
        }

        public void ChangeRating(bool increase)
        {
            if (increase)
            {
                Rating += 1;
            }
            else
            {
                Rating -= 1;
            }
            var sql = $"Update series set WAARDERING = {Rating.ToString()} where SERIENR = {SeriesNr.ToString()}";
            var connectionManager = DefaultManager();
            connectionManager.Update(sql);
        }
    }
}