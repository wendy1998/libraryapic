﻿using System;
using System.Collections.Generic;
using libraryApi.Data;
using libraryApi.Models.BookManagement;

namespace libraryApi.Models.SeriesManagement
{
    public class SeriesManager
    {
        public Dictionary<int, Series> Series { get; } = 
            new Dictionary<int, Series>();

        public SeriesManager()
        {
            LoadSeries();
        }

        private void LoadSeries()
        {
            var connManager = ConnectionManager.DefaultManager();
            var ids = connManager.Select("select SERIENR from series");
            foreach (var id in ids["SERIENR"])
            {
                var idInt = int.Parse(id.ToString());
                if (idInt > 0)
                {
                    Series[idInt] = new Series(idInt);
                }
            }
        }

        public void UpdateSeriesInfo(int seriesId, bool complete, bool boring, bool read, int rating)
        {
            string queryRead = null;
            var query = "update series set";
            var needsUpdate = false;
            var series = Series[seriesId];
            // update object
            if (series.Complete != complete)
            {
                needsUpdate = true;
                series.Complete = complete;
                query = $"{query} COMPLEET = {series.Complete.ToString()}";
            }
            if (series.TooBoring != boring)
            {
                if (needsUpdate)
                {
                    query = $"{query},";
                }
                needsUpdate = true;
                series.TooBoring = boring;
                query = $"{query} TOO_BORING = {series.TooBoring.ToString()}";
            }
            if (series.Read != read)
            {
                // Mark all books in the series as read.
                if (read)
                {
                    queryRead = $"update mijn_boeken set GELEZEN = true where SERIENR = {seriesId.ToString()}";
                    SessionController.OwnedManager ??= new OwnedManager();
                    foreach (var book in BooksManager.SeriesBooks[seriesId])
                    {
                        book.Read = true;
                    }
                }
                
                // Update query string
                if (needsUpdate)
                {
                    query = $"{query},";
                }
                needsUpdate = true;
                series.Read = read;
                query = $"{query} GELEZEN = {series.Read.ToString()}";
            }
            if (series.Rating != rating)
            {
                if (needsUpdate)
                {
                    query = $"{query},";
                }
                needsUpdate = true;
                series.Rating = rating;
                query = $"{query} WAARDERING = {series.Rating.ToString()}";
            }
            
            // Update database
            if (!needsUpdate) return;
            query = $"{query} where SERIENR = {seriesId.ToString()};";
            var manager = ConnectionManager.DefaultManager();
            manager.Update(query);
            // update all books in the series that were not marked as read in the database
            if (queryRead != null)
            {
                manager.Update(queryRead);
            }
        }

        /**
         * Adds new series with existing author
         */
        public Series AddNewSeries(Author author, string seriesName, bool complete, bool tooBoring, bool read, int rating)
        {
            var connManager = ConnectionManager.DefaultManager();
            var query = $"insert into series (AUTEURNR, SERIE, COMPLEET, GELEZEN, TOO_BORING, WAARDERING) " +
                        $"values ('{author.Id.ToString()}', '{seriesName}', '{Convert.ToInt32(complete).ToString()}', " +
                        $"'{Convert.ToInt32(read).ToString()}', '{Convert.ToInt32(tooBoring).ToString()}', '{rating.ToString()}');";
            connManager.Insert(query);
            query = "select max(SERIENR) id from series";
            var output = connManager.Select(query);
            var newId = int.Parse(output["id"][0].ToString());
            var series = new Series(newId, author, seriesName, complete, tooBoring, read, rating);
            Series[newId] = series;
            return series;
        }
    }
}