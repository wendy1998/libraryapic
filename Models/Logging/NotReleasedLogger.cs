﻿using System.Collections.Generic;
using libraryApi.Models.Books;
using libraryApi.Models.Searching;

namespace libraryApi.Models.Logging
{
    /**
     * Normal logger that also stores the results of searches of the not released books.
     */
    public class NotReleasedLogger : Logger
    {
        // Property
        public Dictionary<NotReleasedBook, List<SearchResult>> SearchResults { get; } = 
            new Dictionary<NotReleasedBook, List<SearchResult>>();
    }
}