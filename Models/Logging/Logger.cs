﻿using System;
using System.Collections.Generic;
using System.IO;
using libraryApi.Models.Books;

namespace libraryApi.Models.Logging
{
    /**
     * Used to reconstruct actions on the front-end later on.
     */
    public class Logger
    {
        public Dictionary<string, Dictionary<Book, Book>> Conversions { get; } = 
            new Dictionary<string, Dictionary<Book, Book>>(2)
            {
                {"not_released", new Dictionary<Book, Book>()},
                {"released_book", new Dictionary<Book, Book>()}
            };
        public List<Exception> ExceptionsLog { get; set; } = new List<Exception>();

        // Methods
        /**
         * Log an exception
         * @param e the exception to log
         */
        public void LogException(Exception e) {
            ExceptionsLog.Add(e);
        }
        
        /**
         * write all the errors logged to the provided file.
         * @param filePath the file
         * @throws IOException if something goes wrong
         */
        public void WriteErrorsToFile(string filePath)
        {
            using var writer = new StreamWriter(filePath, append: true);
            foreach (var e in ExceptionsLog)
            {
                writer.WriteLine(e.Message);
            }
        }
        
        /*
         * Logs conversions between book types
         * @param book1: The original book object
         * @param book2: the object that was made when converting the original book.
         */
        public void LogConversion(Book book1, Book book2)
        {
            // not released and released book conversions are kept in separate dictionaries,
            // so the right dictionary has to be used.
            var conversionsMap = Conversions[book1.Type];
            if (conversionsMap.ContainsKey(book1))
            {
                conversionsMap[book1] = book2;
            } else {
                conversionsMap.Add(book1, book2);
            }
            
        }
    }
}