﻿using libraryApi.Data;

namespace libraryApi.Models
{
    /**
     * Represents an author
     */
    public class Author
    {
        // Properties
        public int Id { get; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}".Trim();

        /**
         * constructor
         * @param id: the id of the author in the database
         * @throws DatabaseException if database connectivity goes wrong
         */
        public Author(int id)
        {
            Id = id;
            ExtractName();
        }

        /**
         * constructor
         * @param id: the id of the author in the database
         * @param firstName: first name of the author
         * @param lastName: last name of the author
         */
        public Author(int id, string firstName, string lastName)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
        }

        public Author(string firstName, string initials, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
            Id = AddToDatabase(initials);
        }

        private int AddToDatabase(string initials)
        {
            var connManager = ConnectionManager.DefaultManager();
            // set up sql query
            var queryColumns = "insert into auteurs (";
            var queryValues = "values (";
            
            // firstname was given and thus at least one initial is present too.
            if (!string.IsNullOrEmpty(FirstName))
            {
                queryColumns = $"{queryColumns}VOORNAAM, VOORLETTERS";
                initials ??= FirstName[0].ToString();
                if (!initials.ToLower().StartsWith(FirstName[0].ToString().ToLower()))
                {
                    initials = $"{FirstName[0].ToString()}.{initials}";
                }
                queryValues = $"{queryValues}'{FirstName}', '{initials}'";
                
                // last name was given and needs to be added to query
                if (!string.IsNullOrEmpty(LastName))
                {
                    queryColumns = $"{queryColumns}, ACHTERNAAM";
                    queryValues = $"{queryValues}, '{LastName}'";
                }

                queryColumns = $"{queryColumns})";
                queryValues = $"{queryValues});";
            }
            // no first name
            else
            {
                if (!string.IsNullOrEmpty(initials))
                {
                    queryColumns = $"{queryColumns}VOORLETTERS, ACHTERNAAM)";
                    queryValues = $"{queryValues}'{initials}', '{LastName}');";
                }
                else
                {
                    queryColumns = $"{queryColumns}ACHTERNAAM)";
                    queryValues = $"{queryValues}'{LastName}');";
                }
            }
            var query = $"{queryColumns} {queryValues}";
            
            connManager.Insert(query);
            query = "select max(AUTEURNR) id from auteurs;";
            var output = connManager.Select(query);
            return int.Parse(output["id"][0].ToString());
        }

        /**
         * Extract name of author from database using the id of the author
         * @throws DatabaseException if database connectivity goes wrong
         */
        private void ExtractName()
        {
            var sql = $"select VOORNAAM, ACHTERNAAM from auteurs where AUTEURNR = {Id.ToString()}";
            var manager = ConnectionManager.DefaultManager();
            var output = manager.Select(sql);
            
            FirstName = output["VOORNAAM"][0].ToString();
            LastName = output["ACHTERNAAM"][0].ToString();
        }
        
        public void RemoveFromDatabase()
        {
            var manager = ConnectionManager.DefaultManager();
            var query = $"delete from auteurs where AUTEURNR = {Id.ToString()}";
            manager.Delete(query);
        }

        public override string ToString()
        {
            return FullName;
        }
    }
}