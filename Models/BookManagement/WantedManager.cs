﻿using System;
using System.Collections.Generic;
using libraryApi.Data;
using libraryApi.Models.Books;
using libraryApi.Models.Logging;
using MySql.Data.MySqlClient;

namespace libraryApi.Models.BookManagement
{
    /**
     * Manager of actions with released books
     */
    public class WantedManager : BooksManager
    {
        /**
         * constructor
         */
        public WantedManager() {
            Logger = new Logger();
            LoadBooks();
        }
        
        // Conversions
        /**
         * turns a book I thought I had into a book I want
         * @param ownedBookId: the id of the book I thought I had in the database
         * @param hopeless: whether it is hopeless to look for the book
         * @throws DatabaseException if database connectivity goes wrong
         */
        private static void TurnOwnedIntoWanted(int ownedBookId, bool hopeless) {
            // Use stored procedure to change the book in the database.
            var manager = ConnectionManager.DefaultManager();
            var parameters = new List<MySqlParameter>(2)
            {
                new MySqlParameter("id", ownedBookId.ToString()),
                new MySqlParameter("hopeloos", hopeless.ToString() == "True" ? "1": "0")
            };
            manager.CallStmt("verplaats_heb_ik_naar_wil_ik", parameters);
        }
        
        /**
         * Update the book id of a wanted book because it was turned back
         * @param original the wanted book of which the id needs to be changed
         * @throws DatabaseException if database connectivity goes wrong
         */
        private static void UpdateBookId(WantedBook original) {
            const string sql = "select * from boeken_die_ik_wil where ID = (select max(ID) from boeken_die_ik_wil)";
            var manager = ConnectionManager.DefaultManager();
            var output = manager.Select(sql);

            // collect info for new wanted book from table
            var newId = (int) output["ID"][0];
            // key in dict needs to be changed because the book id was changed.
            WantedBooks.Remove(original.TableId);
            original.TableId = newId;
            WantedBooks.Add(newId, original);
        }
        
        /**
         * converts a wanted book into an owned book
         * @param book: the book to convert
         * @param novel68: whether it was found on novel68
         * @param stillCalibre: whether it has to be added to calibre
         * @param stillEReader: whether it has to be added to my ereader
         * @param read: whether I have read the book
         * @param bookType: "Ebook" or "book"
         * @throws DatabaseException if database connectivity goes wrong
         */
        public void TurnWantedIntoOwned(WantedBook book,
                                 bool novel68=false,
                                 bool stillCalibre=true,
                                 bool stillEReader=true,
                                 bool read=false,
                                 string bookType="Ebook",
                                 bool completesSeries = false) {
            
            // call stored procedure in database to move the book to correct table
            var manager = ConnectionManager.DefaultManager();
            var parameters = new List<MySqlParameter>(2)
            {
                new MySqlParameter("id", book.TableId.ToString()),
                new MySqlParameter("nog_calibre", stillCalibre.ToString().ToLower() == "true" ? "1": "0"),
                new MySqlParameter("nog_ereader", stillEReader.ToString().ToLower() == "true" ? "1": "0"),
                new MySqlParameter("gelezen", read.ToString().ToLower() == "true" ? "1": "0"),
                new MySqlParameter("type", bookType),
                new MySqlParameter("novel68", novel68.ToString().ToLower() == "true" ? "1": "0"),
            };
            manager.CallStmt("verplaats_wil_ik_naar_mijn_boeken", parameters);

            // extract new book id
            var sql = "select * from mijn_boeken where BOEKNR = (select max(BOEKNR) from mijn_boeken)";
            var id = 0;
            try
            {
                var output = manager.Select(sql);
                
                // collect info for new owned book from table
                id = (int) output["BOEKNR"][0];
                var language = output["TAAL"][0].ToString();

                OwnedBook ownedBook;
                if (book.Series != null)
                {
                    ownedBook = new OwnedBook(book.Author, book.Series.SeriesNr,
                        book.NumberInSeries, book.Title, language, id, stillCalibre,
                        stillEReader, novel68: novel68, bookType: bookType, completesSeries: completesSeries);
                }
                else
                {
                    ownedBook = new OwnedBook(book.Author, 0,
                        null, book.Title, language, id, stillCalibre,
                        stillEReader, novel68: novel68, bookType: bookType);
                }

                // remove old book from dictionary with wanted books
                WantedBooks.Remove(book.TableId);
                // add new OwnedBook to dictionary and new book id
                OwnedBooks.Add(id, ownedBook);
                Logger.LogConversion(book, ownedBook);

                if (!completesSeries || book.Series == null) return;
                sql = $"update series set COMPLEET = 1 where SERIENR = {book.Series.SeriesNr.ToString()}";
                manager.Update(sql);
            } catch (Exception e) {
                Logger.LogConversion(book, null);
                // Conversion needs to be undone
                TurnOwnedIntoWanted(id, book.Hopeless);
                UpdateBookId(book);
                Logger.LogException(e);
            }
        }
        
        /**
         * Loads all the wanted books from the database
         * @throws DatabaseException if database connectivity goes wrong
         */
        private void LoadBooks() {
            try {
                // select all the books in the wanted table in the database
                var manager = ConnectionManager.DefaultManager();
                var sql = "select * from boeken_die_ik_wil;";
                var output = manager.Select(sql);
                
                // store them into objects and global dictionary
                WantedBook book;
                var amount = output["ID"].Count;
                for (var i = 0; i < amount; i++)
                {
                    try
                    {
                        var id = (int) output["ID"][i];
                        if (WantedBooks.ContainsKey(id)) continue;
                        var author = new Author(int.Parse(output["AUTEURNR"][i].ToString()));
                        var seriesNr = 0;
                        if (!string.IsNullOrEmpty(output["SERIENR"][i].ToString()))
                        {
                            seriesNr = int.Parse(output["SERIENR"][i].ToString());
                        }

                        string numberInSeries = null;
                        if (seriesNr > 0 && 
                            !string.IsNullOrEmpty(output["NUMMER_IN_SERIE"][i].ToString()))
                        {
                            numberInSeries = output["NUMMER_IN_SERIE"][i].ToString();
                        }

                        var title = output["TITEL"][i].ToString();
                        var language = output["TAAL"][i].ToString();
                        var dateAdded = output["DATUM_TOEGEVOEGD"][i].ToString();
                        var hopeless = (bool) output["HOPELOOS"][i];

                        book = new WantedBook(author, seriesNr, numberInSeries, title, language, id, dateAdded, hopeless);
                        // If it does not exist yet, the book is added to global dictionary.
                        WantedBooks.Add(book.TableId, book);
                    } catch (Exception e) {
                        Logger.LogException(e);
                    }
                }
            } catch (Exception e) {
                Logger.LogException(e);
            }
        }
    }
}