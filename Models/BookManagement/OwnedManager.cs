﻿using System;
using System.Collections.Generic;
using libraryApi.Data;
using libraryApi.Models.Books;
using libraryApi.Models.Exceptions;
using libraryApi.Models.Logging;

namespace libraryApi.Models.BookManagement
{
    public class OwnedManager : BooksManager
    {
        /**
         * constructor
         */
        public OwnedManager() {
            Logger = new Logger();
            try {
                LoadBooks();
            } catch (DatabaseException e) {
                Logger.LogException(e);
            }
        }

        /**
         * Loads all the owned books from the database
         * @throws DatabaseException if database connectivity goes wrong
         */
        private void LoadBooks() {
            try {
                // select all the books in the my books table in the database
                var manager = ConnectionManager.DefaultManager();
                var sql = "select * from mijn_boeken;";
                var output = manager.Select(sql);
                
                // store them into objects and global dictionary
                OwnedBook book;
                var amount = output["BOEKNR"].Count;
                for (var i = 0; i < amount; i++)
                {
                    try
                    {
                        var id = (int) output["BOEKNR"][i];
                        if (OwnedBooks.ContainsKey(id)) continue;
                        var author = new Author(int.Parse(output["AUTEURNR"][i].ToString()));
                        var seriesNr = 0;
                        if (!string.IsNullOrEmpty(output["SERIENR"][i].ToString()))
                        {
                            seriesNr = int.Parse(output["SERIENR"][i].ToString());
                        }

                        string numberInSeries = null;
                        if (seriesNr > 0 && 
                            !string.IsNullOrEmpty(output["NUMMER_IN_SERIE"][i].ToString()))
                        {
                            numberInSeries = output["NUMMER_IN_SERIE"][i].ToString();
                        }

                        var title = output["TITEL"][i].ToString();
                        var language = output["TAAL"][i].ToString();
                        var stillCalibre = (bool) output["NOG_CALIBRE"][i];
                        var stillEreader = (bool) output["NOG_EREADER"][i];
                        var stillBackup = (bool) output["NOG_BACKUP"][i];
                        var novel68 = (bool) output["NOVEL68"][i];
                        var thirdPartyBook = (bool) output["BOEK_VOOR_DERDE"][i];
                        var read = (bool) output["GELEZEN"][i];
                        var tooBoring = (bool) output["TOO_BORING"][i];
                        var hasAudio = (bool) output["HEEFT_AUDIOBOOK"][i];
                        var onlyAudio = (bool) output["ALLEEN_AUDIO"][i];
                        var bookType = output["TYPE"][i].ToString();
                        book = new OwnedBook(author, seriesNr, numberInSeries, title, 
                            language, id, stillCalibre, stillEreader, stillBackup,
                            novel68, thirdPartyBook, read, tooBoring, bookType, hasAudio, onlyAudio);
                        
                        OwnedBooks.Add(book.BookNr, book);
                    } catch (Exception e) {
                        Logger.LogException(e);
                    }
                }
            } catch (Exception e) {
                Logger.LogException(e);
            }
        }

        public void ChangeBook(int bookId, bool boring, bool read, 
            bool calibre, bool ereader, bool backup, bool audio, bool audioOnly)
        {
            var query = "update mijn_boeken set";
            var needsUpdate = false;
            var book = OwnedBooks[bookId];
            
            // update object and fabricate query
            if (book.TooBoring != boring)
            {
                needsUpdate = true;
                book.TooBoring = boring;
                query = $"{query} TOO_BORING = {book.TooBoring.ToString()}";
            }
            if (book.Read != read)
            {
                // Update query string
                if (needsUpdate)
                {
                    query = $"{query},";
                }
                needsUpdate = true;
                book.Read = read;
                query = $"{query} GELEZEN = {book.Read.ToString()}";
            }
            if (book.StillCalibre != calibre)
            {
                // Update query string
                if (needsUpdate)
                {
                    query = $"{query},";
                }
                needsUpdate = true;
                book.StillCalibre = calibre;
                query = $"{query} NOG_CALIBRE = {book.StillCalibre.ToString()}";
            }
            if (book.StillEReader != ereader)
            {
                // Update query string
                if (needsUpdate)
                {
                    query = $"{query},";
                }
                needsUpdate = true;
                book.StillEReader = ereader;
                query = $"{query} NOG_EREADER = {book.StillEReader.ToString()}";
            }
            if (book.StillBackup != backup)
            {
                // Update query string
                if (needsUpdate)
                {
                    query = $"{query},";
                }
                
                needsUpdate = true;
                book.StillBackup = backup;
                query = $"{query} NOG_BACKUP = {book.StillBackup.ToString()}";
            }
            if (book.HasAudiobook != audio)
            {
                // Update query string
                if (needsUpdate)
                {
                    query = $"{query},";
                }

                needsUpdate = true;
                book.HasAudiobook = audio;
                query = $"{query} HEEFT_AUDIOBOOK = {book.HasAudiobook.ToString()}";
            }
            if (book.OnlyAudio != audioOnly)
            {
                // Update query string
                if (needsUpdate)
                {
                    query = $"{query},";
                }

                needsUpdate = true;
                book.OnlyAudio = audioOnly;
                query = $"{query} ALLEEN_AUDIO = {book.HasAudiobook.ToString()}";
            }
            
            // Update database
            if (!needsUpdate) return;
            query = $"{query} where BOEKNR = {bookId.ToString()};";
            var manager = ConnectionManager.DefaultManager();
            manager.Update(query);
        }
    }
}