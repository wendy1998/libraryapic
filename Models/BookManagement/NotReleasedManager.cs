﻿using System;
using System.Collections.Generic;
using System.Linq;
using libraryApi.Data;
using libraryApi.Models.Books;
using libraryApi.Models.Exceptions;
using libraryApi.Models.Logging;
using libraryApi.Models.Searching;
using MySql.Data.MySqlClient;

namespace libraryApi.Models.BookManagement
{
    /**
     * manages everything having to do with not released books
     */
    public class NotReleasedManager : BooksManager
    {
        // Properties
        public new NotReleasedLogger Logger { get; }
        
        /**
         * constructor
         */
        public NotReleasedManager()
        {
            Logger = new NotReleasedLogger();
            try
            {
                LoadBooks();
            }
            catch (DatabaseException e)
            {
                Logger.LogException(e);
            }
        }

        /**
         * update a record with new data from a search result
         * @param book: the book to be modified
         * @param searchResult: the search result containing the new data
         * @param tasks: what to change
         * @return whether it was successful
         */
        public bool UpdateRecord(NotReleasedBook book, SearchResult searchResult, string[] tasks)
        {
            var sql = "update boeken_die_nog_niet_uit_zijn set";
            var needUpdate = false;
            try
            {
                // tasks can contain releaseDate and title
                foreach (var task in tasks)
                {    
                    // add strings to sql query to update release date and update release date on book object.
                    if (task == "releaseDate")
                    {
                        // Construct the string of the found release date from the searchresult fields.
                        var releaseDate = NotReleasedBook.ConstructReleaseDate(
                            searchResult.PublicationYear,
                            searchResult.PublicationMonth,
                            searchResult.PublicationDay
                        );
                        
                        // if the release date was changed, update the sql and set needUpdate to true so database will be updated.
                        var oldRelease = book.ReleaseDate;
                        book.ReleaseDate = releaseDate;
                        if (book.ReleaseDate != null && oldRelease != book.ReleaseDate)
                        {
                            // if title task has already happened, this sql needs a ", " before it, otherwise it does not.
                            sql = sql.Contains("TITEL") ? $"{sql}, DATUM_UITKOMST = '{book.ReleaseDate}'" : $"{sql} DATUM_UITKOMST = '{book.ReleaseDate}'";
                            needUpdate = true;
                        }
                    }
                    
                    // update title if needed
                    if (task != "title") continue;
                    var oldTitle = book.Title;
                    book.Title = searchResult.Title;
                    if (book.Title == null || oldTitle == book.Title) continue;
                    sql = sql.Contains("DATUM_UITKOMST") ? $"{sql}, TITEL = '{book.Title}'" : $"{sql} TITEL = '{book.Title}'";
                    needUpdate = true;
                }
                
                // add book id to sql statement where clause
                sql = $"{sql} where ID = {book.TableId.ToString()}";

                if (!needUpdate) return true;
                ConnectionManager.DefaultManager().Update(sql);
                return true;
            }
            catch (Exception e)
            {
                Logger.LogException(e);
                return false;
            }
        }

        /**
         * update a record with new data.
         * @param book: the book to be modified
         * @param args: key value pairs of fields in the database and their value that need to be updated.
         * @return whether it was successful
         */
        public bool UpdateRecord(NotReleasedBook book, Dictionary<string, string> args)
        {
            var sql = "update boeken_die_nog_niet_uit_zijn set";
            var first = true;
            // Loop through the key, value pairs
            foreach (var (key, value) in args)
            {
                // No ", " needs to be added before the key = value part.
                if (first)
                {
                    sql = $"{sql} {key} = '{value}'";
                    first = false;
                }
                else
                {
                    sql = $"{sql}, {key} = '{value}'";
                }
            }
            
            // Add book id to where clause of query
            sql = $"{sql} where ID = {book.TableId.ToString()}";

            try
            {
                // Try to execute the sql, so book will be updated.
                ConnectionManager.DefaultManager().Update(sql);
                sql =
                    $"select TITEL, DATUM_UITKOMST, NOG_AGENDA FROM boeken_die_nog_niet_uit_zijn where ID = {book.TableId.ToString()}";
                
                // update book object
                // get the title and releaseDate from the database so the object can be updated.
                var output = ConnectionManager.DefaultManager().Select(sql);
                book.Title = output["TITEL"][0].ToString();
                book.ReleaseDate = output["DATUM_UITKOMST"][0].ToString();
                book.StillAgenda = (bool) output["NOG_AGENDA"][0];
                
                return true;
            }
            catch (Exception e)
            {
                Logger.LogException(e);
                return false;
            }
        }

        // conversions

        /**
         * convert a not released book into a book I have
         * @param toReleaseBook the to be converted book
         * @param novel68 whether it was found on novel68 (default false)
         * @param stillCalibre whether it has to be added to calibre (default true)
         * @param stillEReader whether it has to be added to my ereader (default true)
         * @param read whether I have read the book (default false)
         * @param bookType ebook or book (default "Ebook")
         * @throws Exception if something goes wrong
         */
        public void TurnNotReleasedIntoOwned(NotReleasedBook toReleaseBook,
            bool novel68 = false,
            bool stillCalibre = true,
            bool stillEReader = true,
            bool read = false,
            string bookType = "Ebook")
        {
            // First move the book to the wanted table
            TurnNotReleasedIntoWanted(toReleaseBook);
            // Get the Wanted book object created in the process from the logged conversions.
            var newBook = (WantedBook) Logger.Conversions["not_released"][toReleaseBook];
            // Conversion went wrong
            if (newBook == null) return;
            // Use a released manager to turn the newly created wanted book into an owned book.
            var releasedManager = new WantedManager();
            releasedManager.TurnWantedIntoOwned(newBook, novel68, stillCalibre, stillEReader, read, bookType);
        }

        /**
         * turns a wanted book back into a not released book if a conversion failed
         * @param wantedBookId the id of the wanted book to be turned back
         * @param original the book it was initially
         * @throws DatabaseException if database connectivity goes wrong
         */
        private void TurnWantedIntoNotReleased(int wantedBookId, NotReleasedBook original)
        {
            TurnWantedIntoNotReleased(wantedBookId, original.ReleaseDate, original.StillAgenda);
        }

        /**
         * turns a wanted book into a not released book if a mistake was made
         * @param wantedBookId the id of the wanted book to be turned back
         * @param releaseDate the date the non-released book comes out
         * @param stillAgenda whether it needs to be added to the agenda
         * @throws DatabaseException if database connectivity goes wrong
         */
        private void TurnWantedIntoNotReleased(int wantedBookId, string releaseDate, bool stillAgenda = true)
        {
            var connectionManager = ConnectionManager.DefaultManager();
            var parameters = new List<MySqlParameter>(3)
            {
                new MySqlParameter("datum_uitkomst", releaseDate),
                new MySqlParameter("nog_agenda", stillAgenda.ToString()),
                new MySqlParameter("id", wantedBookId.ToString())
            };
            connectionManager.CallStmt("verplaats_wil_ik_naar_niet_uit", parameters);
        }

        /**
         * Turns a not released book into a wanted book
         * @param toReleaseBook the book to be converted
         * @throws Exception if something goes wrong
         */
        private void TurnNotReleasedIntoWanted(NotReleasedBook toReleaseBook)
        {
            var connectionManager = ConnectionManager.DefaultManager();
            // move to wanted table
            try
            {
                // use stored procedure in database to move the book, pass the id of the not released book as parameter
                var parameters = new List<MySqlParameter>(1)
                {
                    new MySqlParameter("id", toReleaseBook.TableId.ToString())
                };
                connectionManager.CallStmt("verplaats_naar_wil_ik_met_id", parameters);
            }
            catch (Exception e)
            {
                Logger.LogException(e);
            }

            // move objects in program
            // Get the newly made book credentials by selecting the book with the highest id in the table with wanted books
            var sql = "select * from boeken_die_ik_wil where ID = (select max(ID) from boeken_die_ik_wil)";
            var id = 0;
            try
            {
                var output = connectionManager.Select(sql);

                // collect info for new wanted book from table
                id = (int) output["ID"][0];
                var language = output["TAAL"][0].ToString();

                // add info to new object and add that to global dictionary with wanted books.
                WantedBook wantedBook;
                if (toReleaseBook.Series != null)
                {
                    wantedBook = new WantedBook(toReleaseBook.Author, toReleaseBook.Series.SeriesNr,
                        toReleaseBook.NumberInSeries, toReleaseBook.Title, language, id);
                }
                else
                {
                    wantedBook = new WantedBook(toReleaseBook.Author, 0,
                        null, toReleaseBook.Title, language, id);
                }

                // updating dictionary and logging
                WantedBooks.Add(id, wantedBook);
                NotReleasedBooks.Remove(toReleaseBook.TableId);
                if (Logger.SearchResults.ContainsKey(toReleaseBook))
                {
                    Logger.SearchResults.Remove(toReleaseBook);
                }
                Logger.LogConversion(toReleaseBook, wantedBook);
            }
            catch (Exception e)
            {
                Logger.LogConversion(toReleaseBook, null);
                // When turning back, book id changes
                TurnWantedIntoNotReleased(id, toReleaseBook);
                UpdateBookId(toReleaseBook);
                Logger.LogException(e);
            }
        }

        /**
         * Update the book if of a not released book book because it was turned back
         * @param original the not released book of which the id needs to be changed
         * @throws DatabaseException if database connectivity goes wrong
         */
        private static void UpdateBookId(NotReleasedBook original)
        {
            var sql =
                "select * from boeken_die_nog_niet_uit_zijn where ID = (select max(ID) from boeken_die_nog_niet_uit_zijn)";
            var manager = ConnectionManager.DefaultManager();
            var output = manager.Select(sql);

            // collect info for new wanted book from table
            var newId = (int) output["ID"][0];
            // key needs to change in global dict.
            NotReleasedBooks.Remove(original.TableId);
            original.TableId = newId;
            NotReleasedBooks.Add(newId, original);
        }

        /**
         * Check if there are loaded books of which the release date has passed and turn them into wanted books
         */
        public void UpdateReleasedBooks() {
            try {
                // cannot directly call turn not released into wanted because then dictionary
                // where the books come from is modified while looping over it
                var toRelease = NotReleasedBooks.Values.Where(book => book.WasReleased()).ToList();

                foreach (var book in toRelease) {
                    TurnNotReleasedIntoWanted(book);
                }
                
            } catch (Exception e) {
                Logger.LogException(e);
            }
        }

        /**
         * Load all unreleased books from the database
         * @throws DatabaseException if database connectivity goes wrong
         */
        private void LoadBooks() {
            try {
                var manager = ConnectionManager.DefaultManager();
                var sql = "select * from boeken_die_nog_niet_uit_zijn;";
                var output = manager.Select(sql);
                
                NotReleasedBook book;
                for (var i = 0; i < output["ID"].Count; i++)
                {
                    int id = (int) output["ID"][i];
                    Author author = new Author(int.Parse(output["AUTEURNR"][i].ToString()));
                    var releaseDate = output["DATUM_UITKOMST"][i].ToString();
                    
                    var seriesNr = 0;
                    if (!string.IsNullOrEmpty(output["SERIENR"][i].ToString()))
                    {
                        seriesNr = int.Parse(output["SERIENR"][i].ToString());
                    }

                    string numberInSeries = null;
                    if (seriesNr > 0 && 
                        !string.IsNullOrEmpty(output["NUMMER_IN_SERIE"][i].ToString()))
                    {
                        numberInSeries = output["NUMMER_IN_SERIE"][i].ToString();
                    }
                    var title = output["TITEL"][i].ToString();
                    var language = output["TAAL"][i].ToString();
                    var stillAgenda = (bool) output["NOG_AGENDA"][i];

                    book = new NotReleasedBook(author, seriesNr, numberInSeries, title, language, releaseDate, stillAgenda, id);
                    NotReleasedBooks.TryAdd(book.TableId, book);
                }
            } catch (Exception e) {
                Logger.LogException(e);
            }
        }

        /**
         * Search for new info on loaded books
         */
        public void SearchForInfoUpdates() {
            try {
                var searchManager = new SearchManager();
                var logger = Logger;
                var searchesLog = logger.SearchResults;
                foreach (var book in NotReleasedBooks.Values) {
                    try {
                        // Look for info updates on goodreads
                        var searchResults = searchManager.PerformSearch(book);
                        searchesLog.Add(book, searchResults);
                    } catch (Exception e) {
                        Logger.LogException(e);
                    }
                }
            } catch (Exception e) {
                Logger.LogException(e);
            }
        }

    }
}