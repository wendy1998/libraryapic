﻿using System.Collections.Generic;
using libraryApi.Models.Books;
using libraryApi.Models.Logging;
using libraryApi.Models.SeriesManagement;

namespace libraryApi.Models.BookManagement
{
    /**
     * Manages all books and stores them
     */
    public abstract class BooksManager
    {
        // Properties
        public static Dictionary<int, OwnedBook> OwnedBooks { get; } = new Dictionary<int, OwnedBook>();
        public static Dictionary<int, List<OwnedBook>> SeriesBooks { get; } = 
            new Dictionary<int, List<OwnedBook>>();
        public static Dictionary<int, WantedBook> WantedBooks { get; } = new Dictionary<int, WantedBook>();
        public static Dictionary<int, NotReleasedBook> NotReleasedBooks { get; } =
            new Dictionary<int, NotReleasedBook>();

        public Logger Logger { get; set; }

        protected BooksManager()
        {
            SessionController.SeriesManager ??= new SeriesManager();
        }
    }
}