﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using libraryApi.Data;
using libraryApi.Models.BookManagement;

namespace libraryApi.Models.Books
{
    /**
     * Represents a book that is owned.
     */
    public class OwnedBook : Book
    {
        // Properties
        public bool HasAudiobook { get; set; }
        public bool OnlyAudio { get; set; }
        public int BookNr { get; set; }
        public bool StillCalibre { get; set; }
        public bool StillEReader { get; set; }
        public bool StillBackup { get; set; }
        private bool Novel68 { get; }
        public bool ThirdPartyBook { get; }
        public bool Read { get; set; }
        public bool TooBoring { get; set; }
        private string BookType { get; }
        private bool CompletesSeries { get; }
        
        /**
         * constructor
         * @param author: the author of the book
         * @param series: the series the book is in (can be 0)
         * @param numberInSeries: the place this book has in its series (can be null)
         * @param title: the title of the book (can be null if book is unpublished)
         * @param language: the language the book was written in
         * @param bookNr: the id of the book in the database
         * @param stillCalibre: whether it needs to be added to calibre, default true
         * @param stillEReader: whether it needs to be added to my ereader. default true
         * @param novel68: whether it was found on novel68, default false
         * @param thirdPartyBook: whether it is for a third party, default false
         * @param read: whether I have read the book, default false
         * @param bookType: Ebook or normal book, default Ebook
         */
        public OwnedBook(Author author,
            int seriesNr,
            string numberInSeries,
            string title,
            string language,
            int bookNr,
            bool stillCalibre = true,
            bool stillEReader = true,
            bool stillBackup = true,
            bool novel68 = false,
            bool thirdPartyBook = false,
            bool read = false,
            bool tooBoring = false,
            string bookType = "Ebook",
            bool hasAudiobook = false,
            bool onlyAudio = false,
            bool completesSeries = false) :
            base ("released_book", author, seriesNr, numberInSeries, title, language) {
                BookNr = bookNr;
                StillCalibre = stillCalibre;
                StillEReader = stillEReader;
                StillBackup = stillBackup;
                Novel68 = novel68;
                ThirdPartyBook = thirdPartyBook;
                Read = read;
                TooBoring = tooBoring;
                BookType = bookType;
                HasAudiobook = hasAudiobook;
                OnlyAudio = onlyAudio;
                
                // add new book to series dictionary
                if (Series == null || Series.SeriesNr == 0) return;
                CompletesSeries = completesSeries;
                if (!Series.Complete)
                {
                    Series.Complete = completesSeries;
                }
                if (BooksManager.SeriesBooks.ContainsKey(seriesNr))
                {
                    BooksManager.SeriesBooks[seriesNr].Add(this);
                }
                else
                {
                    BooksManager.SeriesBooks[seriesNr] = new List<OwnedBook> {this};
                }
        }
        
        // Methods
        public override void AddToDatabase()
        {
            var manager = ConnectionManager.DefaultManager();
            var rgx = new Regex("(['^$.|?*+()\\\\])");
            
            // build query
            var queryColumns = "insert into mijn_boeken (AUTEURNR, TITEL";
            var queryValues = $") values ('{Author.Id.ToString()}', '{rgx.Replace(Title, "\\$1")}'";
            
            if (!string.IsNullOrEmpty(Language) && Language != "Engels")
            {
                queryColumns = $"{queryColumns}, TAAL";
                queryValues = $"{queryValues}, '{Language}'";
            }
            if (ThirdPartyBook)
            {
                queryColumns = $"{queryColumns}, BOEK_VOOR_DERDE";
                queryValues = $"{queryValues}, '1'";
            }
            if (Read)
            {
                queryColumns = $"{queryColumns}, GELEZEN";
                queryValues = $"{queryValues}, '1'";
            }
            if (TooBoring)
            {
                queryColumns = $"{queryColumns}, TOO_BORING";
                queryValues = $"{queryValues}, '1'";
            }
            if (Series != null && Series.SeriesNr != 0)
            {
                queryColumns = $"{queryColumns}, SERIENR";
                queryValues = $"{queryValues}, '{Series.SeriesNr.ToString()}'";
                
                if (!string.IsNullOrEmpty(NumberInSeries))
                {
                    queryColumns = $"{queryColumns}, NUMMER_IN_SERIE";
                    queryValues = $"{queryValues}, '{NumberInSeries}'";
                }
            }
            if (!StillCalibre)
            {
                queryColumns = $"{queryColumns}, NOG_CALIBRE";
                queryValues = $"{queryValues}, '0'";
            }
            if (!StillEReader)
            {
                queryColumns = $"{queryColumns}, NOG_EREADER";
                queryValues = $"{queryValues}, '0'";
            }
            if (!StillBackup)
            {
                queryColumns = $"{queryColumns}, NOG_BACKUP";
                queryValues = $"{queryValues}, '0'";
            }
            if (!string.IsNullOrEmpty(BookType) && BookType.Trim() != "Ebook")
            {
                queryColumns = $"{queryColumns}, TYPE";
                queryValues = $"{queryValues}, '{BookType}'";
            }
            if (Novel68)
            {
                queryColumns = $"{queryColumns}, NOVEL68";
                queryValues = $"{queryValues}, '1'";
            }
            if (HasAudiobook)
            {
                queryColumns = $"{queryColumns}, HEEFT_AUDIOBOOK";
                queryValues = $"{queryValues}, '1'";
            }
            if (OnlyAudio)
            {
                queryColumns = $"{queryColumns}, ALLEEN_AUDIO";
                queryValues = $"{queryValues}, '1'";
            }
            
            // add to database
            var query = $"{queryColumns}{queryValues});";
            manager.Insert(query);
            
            // retrieve new table id
            query = "select max(BOEKNR) id from mijn_boeken;";
            var output = manager.Select(query);
            BookNr = int.Parse(output["id"][0].ToString() ?? "0");

            if (CompletesSeries && Series != null && Series.SeriesNr > 0)
            {
                query = $"update series set COMPLEET = '1' where SERIENR = '{Series.SeriesNr}'";
                manager.Update(query);
            }
        }
    }
}