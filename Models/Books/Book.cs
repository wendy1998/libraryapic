﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using libraryApi.Models.SeriesManagement;

namespace libraryApi.Models.Books
{
    /**
     * Class that represent a book
     */
    public abstract class Book
    {
        // Fields
        private string _type;
        private string _title;

        // Properties
        public string Type
        {
            get => _type;
            set
            {
                // check if passed type is valid
                string[] accepted = {"released_book", "not_released"};
                if (Array.IndexOf(accepted, value) < 0)
                {
                    throw new InvalidDataException(
                        $"Book: setType - invalid type given: {value}. Accepted values: wanted, owned and not_released.");
                }

                _type = value;
            }
        }

        public string Language { get; }
        public string Title
        {
            get => _title;
            set
            {
                // Check if a valid title was given
                if (Type != "not_released" && (string.IsNullOrEmpty(value) || value == "NA"))
                {
                    throw new InvalidDataException(
                        "Book: setTitle - title cannot be null unless the book type is not released!");
                }

                if (string.IsNullOrEmpty(value))
                {
                    _title = "NA";
                    return;
                }

                _title = Regex.Replace(value, "\\(.+\\)", "");
                if (_title.ToLower().Trim() == "Unknown")
                {
                    _title = "NA";
                }
            }
        }
        public string NumberInSeries { get; }
        public Series Series { get; }
        public Author Author { get; }
        
        // constructor
        /**
         * @param type the type of book -> wanted, owned or not released
         * @param author the author of the book
         * @param series the series the book is in (can be null)
         * @param numberInSeries the place this book has in its series (can be null)
         * @param title the title of the book (can be null if book is unpublished)
         * @param language the language the book was written in
         */
        protected Book(string type, Author author, 
            int seriesNr = 0, string numberInSeries = null,
            string title = null, string language = "Engels")
        {
            Type = type;
            Author = author;
            if (seriesNr > 0)
            {
                SessionController.SeriesManager ??= new SeriesManager();
                Series = SessionController.SeriesManager.Series[seriesNr];
                NumberInSeries = numberInSeries;
            }
            Title = title;
            Language = language;
        }
        
        // Methods
        public override string ToString()
        {
            // author - ?
            var output = $"{Author.FullName} -";
            if (Series != null && Series.SeriesNr > 0)
            {    
                // author - serie ?
                output = $"{output} {Series.Name.TrimEnd()}";
                if (NumberInSeries != null)
                {
                    // author - serie serienr: title
                    output = $"{output} {NumberInSeries}";
                }
                
                // author - serie: title
                output = $"{output}: {Title}";
            }
            else
            {   
                // author - title
                output = $"{output} {Title}";
            }

            return output;
        }

        public abstract void AddToDatabase();
    }
}