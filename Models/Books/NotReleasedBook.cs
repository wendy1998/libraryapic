﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using libraryApi.Data;
using libraryApi.Models.BookManagement;

namespace libraryApi.Models.Books
{
    /**
     * Represents a book that wasn't released yet
     */
    public class NotReleasedBook : Book
    {    
        // Fields
        private string _releaseDate;
        
        // Properties
        public string ReleaseDate
        {
            get => _releaseDate;
            set
            {
                // either yyyy-mm-dd or yyyy-mm or yyyy
                if (value != null && (Regex.IsMatch(value, "^\\d{4}-\\d{2}-\\d{2}") ||
                                      Regex.IsMatch(value,"^\\d{4}-\\d{2}") ||
                                      Regex.IsMatch(value,"^\\d{4}")))
                {
                    _releaseDate = value;
                }
                else
                {
                    _releaseDate = null;
                }
            }
        }
        public bool StillAgenda { get; set; }
        public int TableId { get; set; }

        /**
         * constructor
         * @param author: the author of the book
         * @param series: the series the book is in (can be null)
         * @param numberInSeries: the place this book has in its series (can be null)
         * @param title: the title of the book (can be null)
         * @param language: the language the book was written in
         * @param releaseDate: the date the book can be released on (can be null)
         * @param stillAgenda: whether it needs to be added to the agenda
         * @param tableId: the id the book has in the database
         */
        public NotReleasedBook(Author author,
            int seriesNr,
            string numberInSeries,
            string title,
            string language,
            string releaseDate,
            bool stillAgenda,
            int tableId) : base("not_released", author, seriesNr, numberInSeries, title, language)
        {
            ReleaseDate = releaseDate;
            StillAgenda = stillAgenda;
            TableId = tableId;
        }
        
        /**
         * whether all the information (both release date to the day and title) is present for this book
         * @return true or false
         */
        private bool IsComplete()
        {
            return !string.IsNullOrEmpty(ReleaseDate) &&
                   Regex.IsMatch(ReleaseDate, "^\\d{4}-\\d{2}-\\d{2}") && 
                   !string.IsNullOrEmpty(Title) && Title != "NA";
        }

        /**
         * whether the release date has passed
         * @return true or false
         */
        public bool WasReleased()
        {
            if (!IsComplete()) return false;
            var parsed = DateTime.ParseExact(ReleaseDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            var now = DateTime.Today;
            return DateTime.Compare(parsed, now) <= 0;
        }

        /**
         * construct a release date from years, months and days
         * @param year the year
         * @param month the month
         * @param day the day
         * @return a string combining all elements that weren't null
         */
        public static string ConstructReleaseDate(int year, int month, int day)
        {
            if (year == 0) return null;
            var output = $"{year.ToString()}";
            
            if (month == 0) return output;
            output = month.ToString().Length == 1 ? $"{output}-0{month.ToString()}" : $"{output}-{month.ToString()}";
            
            if (day == 0) return output;
            output = day.ToString().Length == 1 ? $"{output}-0{day.ToString()}" : $"{output}-{day.ToString()}";
            
            return output;
        }

        public void UpdateDatabase(bool agenda = false, bool title = false, bool releaseDate = false)
        {
            var rgx = new Regex("(['^$.|?*+()\\\\])");
            // set up update query
            var query = "update boeken_die_nog_niet_uit_zijn set ";
            bool needsUpdate = false;
            if (title)
            {
                query = $"{query}TITEL = '{rgx.Replace(Title, "\\$1")}'";
                needsUpdate = true;
            }
            if (releaseDate)
            {
                if (needsUpdate)
                {
                    query = $"{query}, ";
                }
                query = $"{query}DATUM_UITKOMST = ";
                query = !string.IsNullOrEmpty(ReleaseDate) ? $"{query}'{ReleaseDate}'" : $"{query}'onbekend'";
                needsUpdate = true;
            }
            if (agenda)
            {
                if (needsUpdate)
                {
                    query = $"{query}, ";
                }

                var stillAgenda = "0";
                if (StillAgenda)
                {
                    stillAgenda = "1";
                }
                query = $"{query}NOG_AGENDA = '{stillAgenda}'";
            }
            query = $"{query} where ID = {TableId.ToString()};";

            var manager = ConnectionManager.DefaultManager();
            manager.Update(query);
        }

        public override void AddToDatabase()
        {
            var manager = ConnectionManager.DefaultManager();
            var rgx = new Regex("(['^$.|?*+()\\\\])");
            
            // build query
            var queryColumns = "insert into boeken_die_nog_niet_uit_zijn (AUTEURNR";
            var queryValues = $") values ('{Author.Id.ToString()}'";

            if (!string.IsNullOrEmpty(ReleaseDate))
            {
                queryColumns = $"{queryColumns}, DATUM_UITKOMST";
                queryValues = $"{queryValues}, '{ReleaseDate}'";
            }
            if (Series != null && Series.SeriesNr != 0)
            {
                queryColumns = $"{queryColumns}, SERIENR";
                queryValues = $"{queryValues}, '{Series.SeriesNr.ToString()}'";
                
                if (!string.IsNullOrEmpty(NumberInSeries))
                {
                    queryColumns = $"{queryColumns}, NUMMER_IN_SERIE";
                    queryValues = $"{queryValues}, '{NumberInSeries}'";
                }
            }
            if (!string.IsNullOrEmpty(Title) && Title != "NA")
            {
                queryColumns = $"{queryColumns}, TITEL";
                var escaped =
                queryValues = $"{queryValues}, '{rgx.Replace(Title, "\\$1")}'";
            }
            if (!string.IsNullOrEmpty(Language) && Language != "Engels")
            {
                queryColumns = $"{queryColumns}, TAAL";
                queryValues = $"{queryValues}, '{Language}'";
            }
            if (StillAgenda)
            {
                queryColumns = $"{queryColumns}, NOG_AGENDA";
                queryValues = $"{queryValues}, '1'";
            }

            // add to database
            var query = $"{queryColumns}{queryValues});";
            manager.Insert(query);
            
            // retrieve new table id
            query = "select max(ID) id from boeken_die_nog_niet_uit_zijn;";
            var output = manager.Select(query);
            TableId = int.Parse(output["id"][0].ToString() ?? "0");
        }
    }
}