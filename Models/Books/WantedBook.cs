﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using libraryApi.Data;

namespace libraryApi.Models.Books
{
    public class WantedBook : Book
    {
        // Properties
        public int TableId { get; set; }
        private DateTime? DateAdded { get; }
        public bool Hopeless { get; set; }

        /**
         * constructor
         * @param author: the author of the book
         * @param series: the series the book is in (can be null)
         * @param numberInSeries: the place this book has in its series (can be null)
         * @param title: the title of the book (can be null if book is unpublished)
         * @param language: the language the book was written in
         * @param tableId: the id of the book in the database
         * @param dateAdded: the date it was added to the wanted table
         * @param hopeless: whether it is hopeless and there is no use in searching for it
         */
        public WantedBook(Author author,
            int seriesNr,
            string numberInSeries,
            string title,
            string language,
            int tableId,
            string dateAdded,
            bool hopeless) : base("released_book", author, seriesNr, numberInSeries, title, language) {
            TableId = tableId;
            // ReSharper disable once PossibleInvalidOperationException
            DateAdded = SetDateAdded(dateAdded);
            Hopeless = hopeless;
        }

        /**
         * constructor
         * @param author: the author of the book
         * @param series: the series the book is in (can be null)
         * @param numberInSeries: the place this book has in its series (can be null)
         * @param title: the title of the book (can be null if book is unpublished)
         * @param language: the language the book was written in
         * @param tableId: the id of the book in the database
         */
        public WantedBook(
            Author author, 
            int seriesNr, 
            string numberInSeries, 
            string title, 
            string language,
            int tableId) : base("released_book", author, seriesNr, numberInSeries, title, language) {
            TableId = tableId;
            DateAdded = DateTime.Now;
            Hopeless = false;
        }

        // Methods
        public override void AddToDatabase()
        {
            var manager = ConnectionManager.DefaultManager();
            var rgx = new Regex("(['^$.|?*+()\\\\])");

            // build query
            var queryColumns = "insert into boeken_die_ik_wil (AUTEURNR";
            var queryValues = $") values ('{Author.Id.ToString()}'";
            
            if (Series != null && Series.SeriesNr != 0)
            {
                queryColumns = $"{queryColumns}, SERIENR";
                queryValues = $"{queryValues}, '{Series.SeriesNr.ToString()}'";
                
                if (!string.IsNullOrEmpty(NumberInSeries))
                {
                    queryColumns = $"{queryColumns}, NUMMER_IN_SERIE";
                    queryValues = $"{queryValues}, '{NumberInSeries}'";
                }
            }
            if (!string.IsNullOrEmpty(Title) && Title != "NA")
            {
                queryColumns = $"{queryColumns}, TITEL";
                queryValues = $"{queryValues}, '{rgx.Replace(Title, "\\$1")}'";
            }
            if (!string.IsNullOrEmpty(Language) && Language != "Engels")
            {
                queryColumns = $"{queryColumns}, TAAL";
                queryValues = $"{queryValues}, '{Language}'";
            }

            if (DateAdded != null)
            {
                queryColumns = $"{queryColumns}, DATUM_TOEGEVOEGD";
                queryValues = $"{queryValues}, '{DateAdded.Value:yyyy-MM-dd}'";
            }
            if (Hopeless)
            {
                queryColumns = $"{queryColumns}, HOPELOOS";
                queryValues = $"{queryValues}, '1'";
            }

            // add to database
            var query = $"{queryColumns}{queryValues});";
            manager.Insert(query);
            
            // retrieve new table id
            query = "select max(ID) id from boeken_die_ik_wil;";
            var output = manager.Select(query);
            TableId = int.Parse(output["id"][0].ToString() ?? "0");
        }
        
        /**
         * Construct datetime object from string date given.
         */
        private static DateTime? SetDateAdded(string dateAdded)
        {
            if (dateAdded != "NA")
            {
                return DateTime.ParseExact(dateAdded, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }

            return null;
        }

        public void MarkAsHopeless()
        {
            Hopeless = true;
            var query = $"update boeken_die_ik_wil set HOPELOOS = '1' where ID = {TableId.ToString()}";
            var manager = ConnectionManager.DefaultManager();
            manager.Update(query);
        }
    }
}