﻿using System.Collections.Generic;
using libraryApi.Models.BookManagement;
using libraryApi.Models.Logging;
using libraryApi.Models.SeriesManagement;

namespace libraryApi.Models
{
    public static class SessionController
    {
        public static NotReleasedManager NotReleasedManager { get; set; }
        public static OwnedManager OwnedManager { get; set; }
        public static SeriesManager SeriesManager { get; set; }
        public static NotReleasedLogger ActionsLog { get; set; }
        public static bool RemoveCreds { get; set; }
        public static Dictionary<string, string> DatabaseCreds { get; set; }

        public static IEnumerable<Dictionary<string, string>> MenuItems { get; } =
            new List<Dictionary<string, string>>
            {
                new Dictionary<string, string>(2)
                {
                    {"page", "/Index"},
                    {"name", "Home"}
                },
                new Dictionary<string, string>(2)
                {
                    {"page", "/NotReleased"},
                    {"name", "Not Released"}
                },
                new Dictionary<string, string>(2)
                {
                    {"page", "/Wanted"},
                    {"name", "Wanted Books"}
                },
                new Dictionary<string, string>(2)
                {
                    {"page", "/Owned"},
                    {"name", "Owned Books"}
                },
                new Dictionary<string, string>(2)
                {
                    {"page", "/SeriesPicker"},
                    {"name", "Series Generator"}
                },
                new Dictionary<string, string>(2)
                {
                    {"page", "/SeriesPage"},
                    {"name", "Series management"}
                },
                new Dictionary<string, string>(2)
                {
                    {"page", "/AddBooks"},
                    {"name", "Add Books"}
                }
            };
    }
}