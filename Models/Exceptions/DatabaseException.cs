﻿using System;

namespace libraryApi.Models.Exceptions
{
    /**
     * Class representing an exception for if something goes wrong while communicating with the database.
     */
    public class DatabaseException : Exception {

        public DatabaseException(string message) : base(message)
        {
            
        }
    }
}