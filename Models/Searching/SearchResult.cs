﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace libraryApi.Models.Searching
{
    /**
     * Stores information of a goodreads search
     */
    public class SearchResult
    {
        // Properties
        private string Method { get; }
        public bool Found { get; set; } = true;
        public string Title { get; set; }
        public int PublicationMonth { get; set; }
        public int PublicationDay { get; set; }
        public int PublicationYear { get; set; }

        /**
         * constructor
         * @param method either full search or title search
         * @param data the xml representation of the data found in the search.
         */
        public SearchResult(string method, XDocument data) {
            Method = method;
            ParseXml(data);
        }
        
        /**
         * parse all the information from the provided data
         * @param data the provided data
         */
        private void ParseXml(XDocument data) {
            // the fields in the xml will differ depending on the searchmethod used
            if (Method.Contains("title"))
            {
                // a book field was found
                if (data.Descendants("book").ToList().Count > 0)
                {
                    // store the book as a new XElement
                    var book = data.Descendants("book").ToArray()[0];
                    if (book != null) {
                        // the book has a title property
                        if (book.Descendants("title").ToList().Count > 0)
                        {
                            var title = book.Descendants("title").ToList()[0];
                            if (title.Value != "<title></title>")
                            {
                                // remove the xml tags from the value, so title can be stored.
                                Title = title.Value.Replace("<title>", 
                                    "").Replace("</title>", 
                                    "");
                            }
                        }
                        
                        // publication date has to be extracted from "work" element of the "book" element
                        if (book.Descendants("work").ToList().Count <= 0) return;
                        var work = book.Descendants("work").ToList()[0];
                        if (work.HasElements)
                        {
                            ExtractPublicationDate(work);
                        }
                    } else {
                        Found = false;
                    }
                } else {
                    Found = false;
                }
            } else
            {
                var searches = data.Descendants("search").ToList();
                if (searches.Count > 0)
                {
                    // get the results of the first searches if there were any searches
                    var results = searches[0].Descendants("results").ToList();
                    if (results.Count > 0)
                    {
                        // if the search yielded a result, get the "works" in that result
                        var works = results[0].Descendants("work").ToList();
                        if (works.Count > 0)
                        {
                            // if there was a work, store publication date of that work in the searchResult.
                            var work = works[0];
                            ExtractPublicationDate(work);
                            // get the book in that work and store more information
                            var bestBooks = work.Descendants("best_book").ToList();
                            if (bestBooks.Count <= 0) return;
                            var titles = bestBooks[0].Descendants("title").ToList();
                            if (titles.Count <= 0) return;
                            var title = titles[0];
                            if (title.Value != "<title></title>")
                            {
                                Title = title.Value.Replace("<title>", "").Replace("</title>", "");
                            }
                        } else {
                            Found = false;
                        }
                    } else {
                        Found = false;
                    }
                } else {
                    Found = false;
                }
            }
        }
        
        /**
         * extract full date from the three date parts in the data
         * @param work the xml data the date needs to be extracted from.
         */
        private void ExtractPublicationDate(XElement work)
        {
            // check if publication day was found
            var pubDays = work.Descendants("original_publication_day").ToList();
            if (pubDays.Count > 0) {
                // the publication day was found
                if (pubDays[0].Attribute("nil") == null)
                {
                    PublicationDay = Int32.Parse(Regex.Replace(pubDays[0].Value, 
                        "<.+?>", ""));
                }
            }
            
            // check if publication month was found
            var pubMonths = work.Descendants("original_publication_month").ToList();
            if (pubMonths.Count > 0) {
                // the publication month was found
                if (pubMonths[0].Attribute("nil") == null)
                {
                    PublicationMonth = Int32.Parse(Regex.Replace(pubMonths[0].Value, 
                        "<.+?>", ""));
                }
            }
            
            // check if publication year was found
            var pubYears = work.Descendants("original_publication_year").ToList();
            if (pubYears.Count <= 0) return;
            // the publication year was found
            if (pubYears[0].Attribute("nil") == null)
            {
                PublicationYear = Int32.Parse(Regex.Replace(pubYears[0].Value, 
                    "<.+?>", ""));
            }
        }
        
        public override string ToString() {
            return $"SearchResult{{method={Method}, " +
                   $"found={Found.ToString()}, " +
                   $"title={Title}, " +
                   $"publicationDay={PublicationDay.ToString()}, " +
                   $"publicationMonth={PublicationMonth.ToString()}, " +
                   $"publicationYear={PublicationYear.ToString()}}}";
        }
    }
}