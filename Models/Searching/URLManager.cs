﻿using System.Collections.Generic;
using System.IO;

namespace libraryApi.Models.Searching
{
    /**
     * Manages urls and connections to goodreads
     */
    public class UrlManager
    {
        // Properties
        private Dictionary<string, string> ConnectionProperties { get; }

        /**
         * Constructor
         */
        public UrlManager()
        {
            try {
                ConnectionProperties = new Dictionary<string, string>();
                foreach (var line in File.ReadAllLines($"{Program.ContentRoot}/Properties/goodReadsCreds.properties"))
                {
                    var splt = line.Split("=");
                    ConnectionProperties[splt[0].Trim()] = splt[1].Trim();
                }
            } catch (IOException ex) {
                throw new IOException($"URLManager - IOException: {ex.Message}");
            }
        }

        /**
         * stores the base url for title search
         * @return the url
         */
        private string GetTitleSearchUrl() {
            return $"https://www.goodreads.com/book/title.xml?key={ConnectionProperties["key"]}";
        }

        /**
         * stores the base url for full search
         * @return the url
         */
        private string GetFullSearchUrl() {
            return $"https://www.goodreads.com/search/index.xml?key={ConnectionProperties["key"]}&q=";
        }

        /**
         * constructs an url specific for data from the book described in the parameters
         * @param author: the author of the book
         * @param title: the title of the book
         * @param fullSearch: whether to do a full search or a title search
         * @return the url for that book
         */
        public string GetFormattedUrl(string author, string title, bool fullSearch)
        {
            // add authors and titles to the correct base url.
            if (fullSearch) {
                var baseUrl = GetFullSearchUrl();
                return $"{baseUrl}{author.Replace(" ", "+")}%20{title.Replace(" ", "+")}".Replace("'", "%27");
            }

            var toFormat = $"{GetTitleSearchUrl()}&title={title.Replace(" ", "+")}";
            return author != null ? $"{toFormat}&author={author.Replace(" ", "+")}".Replace("'", "%27") : toFormat.Replace("'", "%27");
        }
    }
}