﻿using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Xml.Linq;
using libraryApi.Models.Books;

namespace libraryApi.Models.Searching
{
    /**
     * Performs everything having to do with goodreads searches.
     */
    public class SearchManager
    {
        /**
         * performs search with the provided book as basis
         * @param book the (incomplete) book to search for
         * @return two searchresults, one for a title search and one for a full search
         * @throws Exception if something goes wrong
         */
        public List<SearchResult> PerformSearch(Book book) {
            var urlManager = new UrlManager();
            string urlFull;
            string urlTitle;
            // get the urls for the two types of searches
            if (string.IsNullOrEmpty(book.Title)) {
                if (book.Series != null && book.Series.SeriesNr > 0) {
                    if (book.NumberInSeries != null) {
                        urlFull = urlManager.GetFormattedUrl(book.Author.FullName,
                            $"{book.Series.Name} {book.NumberInSeries} - {book.Title}", true);
                        urlTitle = urlManager.GetFormattedUrl(book.Author.FullName,
                            $"{book.Series.Name} {book.NumberInSeries} - {book.Title}", false);
                    } else {
                        urlFull = urlManager.GetFormattedUrl(book.Author.FullName,
                            $"{book.Series.Name} - {book.Title}", true);
                        urlTitle = urlManager.GetFormattedUrl(book.Author.FullName,
                            $"{book.Series.Name} - {book.Title}", false);
                    }
                } else {
                    urlFull = urlManager.GetFormattedUrl(book.Author.FullName,
                        book.Title, true);
                    urlTitle = urlManager.GetFormattedUrl(book.Author.FullName,
                        book.Title, false);
                }
            } else {
                urlFull = urlManager.GetFormattedUrl(book.Author.FullName,
                    $"{book.Series.Name} {book.NumberInSeries}", true);
                urlTitle = urlManager.GetFormattedUrl(book.Author.FullName,
                    $"{book.Series.Name} {book.NumberInSeries}", false);
            }
            
            // get the xml contents returned by the url
            var outputTitle  = GetUrlContents(urlTitle);
            var outputFull  = GetUrlContents(urlFull);
            
            // turn the xml contents into searchresult objects if anything was returned.
            var output = new List<SearchResult>(2);
            if (outputTitle != null) {
                var titleSearch = new SearchResult("title_search", outputTitle);
                output.Add(titleSearch);
            } else {
                output.Add(null);
            }
            if(outputFull != null) {
                var fullSearch = new SearchResult("full_search", outputFull);
                output.Add(fullSearch);
            } else {
                output.Add(null);
            }

            return output;
        }
        
        /**
         * collects all the data goodreads has for the url provided
         * @param theUrl: the url from which data has to be collected
         * @return an XML representation of the response
         */
        private static XDocument GetUrlContents(string theUrl)
        {
            try
            {
                byte[] data;
                using (var webClient = new WebClient())
                    data = webClient.DownloadData(theUrl);

                var str = Encoding.Default.GetString(data);
                return XDocument.Parse(str);
            } catch
            {
                return null;
            }
        }
    }
}