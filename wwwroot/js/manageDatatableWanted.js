﻿if (document.readyState) {
    // create table
    const table = $('#datatable').DataTable(
        {
            rowGroup: {
                dataSrc: 0
            }
        }
    );

    table.on('click', ".searchButton", function () {
        show_searches(table, this)
    })
}

function format (searchString) {
    // urls of search locations and their names
    const urls = {
        "Ebook Hunter": "https://www.ebookhunter.net/?s=" + searchString,
        "Free paid books": "https://freepaidbooks.online/?s=" + searchString,
        "NZB index": "https://nzbindex.nl/?q=" + searchString,
        "NZB finder": "https://nzbfinder.ws/search?id=" + searchString,
        "Sky torrents": "https://www.skytorrents.lol/?query=" + searchString,
        "Bin search": "https://www.binsearch.info/?q=" + searchString
    };
    const keys = Object.keys(urls);
    const values = Object.values(urls);
    let tableList = ['<table class="table-bordered table-responsive">' +
    '<tr>'];

    // <td><a href=url class=btn target=_blank>website</a></td>
    for(let i=0; i < values.length; i++) {
        tableList.push(
            '<td>', 
            "<a href=\"",
            values[i], 
            "\" class=\"btn\" target=\"_blank\">", 
            keys[i], 
            "</a>", 
            '</td>')
    }

    tableList.push(
        '</tr>' +
        '</table>'
    );
    
    return tableList.join("");
}

const show_searches = function (table, button) {
    const tr = $(button).closest("tr", table);
    const row = table.row(tr);

    if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
        button.innerText = "Search for book"
    }
    else {
        row.child(format($(button).val())).show();
        tr.addClass('shown');
        button.innerText = "Close search row"
    }
};