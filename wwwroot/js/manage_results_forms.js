﻿const manageVisibility = function() {
    const method = $("#updateSelector :selected").val();
    const resultDiv = $("#result");
    const customDiv = $("#custom");

    if (method === "result") {
        if (resultDiv.hasClass("hidden")) {
            resultDiv.removeClass("hidden");
        }
        if (!customDiv.hasClass("hidden")) {
            customDiv.addClass("hidden");
        }
    } else {
        if (customDiv.hasClass("hidden")) {
            customDiv.removeClass("hidden");
        }
        if (!resultDiv.hasClass("hidden")) {
            resultDiv.addClass("hidden");
        }
    }
};

if (document.readyState) {
    var selector = document.getElementById("updateSelector");
    selector.addEventListener("change", manageVisibility);
    manageVisibility();
}