﻿if (document.readyState) {
    // create table
    const table = $('#datatable').DataTable();

    table.on('click', ".addButton", function () {
        show_buttons(table, this)
    })
}

function format (seriesNr) {
    const tableList = ['<table class="table-bordered table-responsive">' +
    '<tr>' +
    '<td>' +
    '<form>' +
    '<input type="hidden" name="type" value="notReleased">' +
    '<input type="hidden" name="seriesNr" value="' + seriesNr + '">' +
    '<input type="submit" value="Add not released book" formaction="/SeriesPage/AddBook">' +
    '</form>' +
    '</td>' +
    '<td>' +
    '<form>' +
    '<input type="hidden" name="type" value="wanted">' +
    '<input type="hidden" name="seriesNr" value="' + seriesNr + '">' +
    '<input type="submit" value="Add wanted book" formaction="/SeriesPage/AddBook">' +
    '</form>' +
    '</td>' +
    '<td>' +
    '<form>' +
    '<input type="hidden" name="type" value="owned">' +
    '<input type="hidden" name="seriesNr" value="' + seriesNr + '">' +
    '<input type="submit" value="Add owned book" formaction="/SeriesPage/AddBook">' +
    '</form>' +
    '</td>' +
    '</tr>' +
    '</table>'];
    return tableList.join("");
}

const show_buttons = function (table, button) {
    const tr = $(button).closest("tr", table);
    const row = table.row(tr);

    if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
        button.innerText = "Add book"
    }
    else {
        row.child(format(button.id)).show();
        tr.addClass('shown');
        button.innerText = "Cancel"
    }
};