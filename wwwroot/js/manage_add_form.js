﻿const manageVisibility = function(id) {
    const to_manage = $("#" + id);
    if (to_manage.hasClass("hidden")) {
        to_manage.removeClass("hidden");
    } else {
        to_manage.addClass("hidden");
    }
};

const toggleBooktypeVisibility = function (value) {
    const releasedDiv = $("#releasedDiv")
    const notReleasedDiv = $("#notReleasedDiv")
    if (value === "wanted") {
        if (!releasedDiv.hasClass("hidden")) {
            manageVisibility("releasedDiv")
        }
        if (!notReleasedDiv.hasClass("hidden")) {
            manageVisibility("notReleasedDiv")
        }
    } else {
        if (value === "notReleased") {
            if (!releasedDiv.hasClass("hidden")) {
                manageVisibility("releasedDiv")
            }
            if (notReleasedDiv.hasClass("hidden")) {
                manageVisibility("notReleasedDiv")
            }
        } else {
            if (releasedDiv.hasClass("hidden")) {
                manageVisibility("releasedDiv")
            }
            if (!notReleasedDiv.hasClass("hidden")) {
                manageVisibility("notReleasedDiv")
            }
        }
    }
}

if (document.readyState) {
    $("#booktype_selector").on("change", function () {
        toggleBooktypeVisibility($("#booktype_selector :selected").val())
    });
    
    $("#language_selector").on("change", function () {
        const selected = $("#language_selector :selected").val();
        const input = $("#language_input");
        input.val(selected)
        if (selected === "Other") {
            if (input.hasClass("hidden")) {
                manageVisibility("language_input")
            }
        } else {
            if (!input.hasClass("hidden")) {
                manageVisibility("language_input")
            }
        }
    });
    
}

