﻿if (document.readyState) {
    // create tables
    $('#datatable_agenda').DataTable();
    $('#datatable_calibre').DataTable(
        {
            rowGroup: {
                dataSrc: 1
            }
        }
    );
    $('#datatable_backup').DataTable(
        {
            rowGroup: {
                dataSrc: 1
            }
        }
    );
    $('#datatable_ereader').DataTable(
        {
            rowGroup: {
                dataSrc: 1
            }
        }
    );
    $('#datatable_released').DataTable();
}