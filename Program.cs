using System;
using System.Collections.Generic;
using System.IO;
using libraryApi.Data;
using libraryApi.Models;
using libraryApi.Models.BookManagement;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace libraryApi
{
    /**
     * Main class
     */
    public class Program
    {
        public static string ContentRoot { get; } = "C:/Users/wendy/Documents/websites/boekenApi/C#";
        /**
         * Application starting point
         */
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
            // SessionController.DatabaseCreds = new Dictionary<string, string>()
            // {
            //     ["server"] = "127.0.0.1",
            //     ["database"] = "bibliotheek",
            //     ["username"] = "dev",
            //     ["password"] = "development"
            // };
        }

        /**
         * Creates host for front-end
         */
        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseContentRoot(ContentRoot);
                });
        }
    }
}
