﻿#nullable enable
using System;
using System.Collections.Generic;
using libraryApi.Models;
using libraryApi.Models.BookManagement;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace libraryApi.Pages
{
    public class Wanted : PageModel
    {
        // Properties
        public bool Error { get; set; }
        public string? ErrorMessage { get; set; }
        public bool Success { get; set; }
        public string? SuccessMessage { get; set; }
        
        // Information for page styling
        public Dictionary<string, string> HeaderItems { get; } =
            new Dictionary<string, string>(2)
            {
                {"active", "Wanted Books"},
                {"header", "Wanted books management"}
            };

        public List<List<string>> Scripts { get; } = new List<List<string>>
        {
            new List<string>
            {
                "/js/manageDatatableWanted.js",
                "text/javascript"
            }
        };
        
        // Page handlers
        public void OnGet()
        {
            if (SessionController.DatabaseCreds == null) return;
            try
            {
                var wantedManager = new WantedManager();
                if (wantedManager.Logger.ExceptionsLog.Count <= 0) return;
                Error = true;
                Success = false;
                wantedManager.Logger.WriteErrorsToFile($"{Program.ContentRoot}/wwwroot/Logfile.txt");
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
                Error = true;
                Success = false;
            }
            finally
            {
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }
            }
        }
        
        public void OnPostHopeless(int bookId)
        {
            try
            {
                var wantedBook = BooksManager.WantedBooks[bookId];
                wantedBook.MarkAsHopeless();
                Success = true;
                SuccessMessage = $"Marked {wantedBook} as hopeless.";
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
                Error = true;
                Success = false;
            }
            finally
            {
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }
            }
        }

        public void OnPostFound(int bookId, bool completesSeries)
        {
            try
            {
                var wantedManager = new WantedManager();
                wantedManager.TurnWantedIntoOwned(BooksManager.WantedBooks[bookId], 
                    completesSeries: completesSeries);
                Success = true;
                SuccessMessage = $"Added wanted book with id {bookId.ToString()} to owned books.";
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
                Error = true;
                Success = false;
            }
            finally
            {
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }
            }
        }

        public IActionResult OnPostAdd(int bookId)
        {
            AddBooks.Author = BooksManager.WantedBooks[bookId].Author;
            AddBooks.FromBooks = true;
            
            return RedirectToPage("AddBooks");
        }
    }
}