﻿#nullable enable
using System;
using System.Collections.Generic;
using libraryApi.Models;
using libraryApi.Models.SeriesManagement;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace libraryApi.Pages
{
    public class SeriesPage : PageModel
    {
        // Information for page styling
        public Dictionary<string, string> HeaderItems { get; } =
            new Dictionary<string, string>(2)
            {
                {"active", "Series management"},
                {"header", "Manage series"}
            };
        
        public bool Error { get; set; }
        public string? ErrorMessage { get; set; }
        public bool Success { get; set; }
        public List<List<string>> Scripts { get; } = new List<List<string>>
        {
            new List<string>
            {
                "/js/manageDatatableSeries.js",
                "text/javascript"
            }
        };
        public string? SuccessMessage { get; set; } = null;

        public static Author? AuthorNewSeries { get; set; }
        public Series? SeriesToModify { get; set; }

        // Page handlers
        public void OnGet()
        {
            
        }

        /**
         * Pass series information to addBook page and redirect to addBooks page after.
         */
        public IActionResult OnGetAddBook(string type, int seriesNr)
        {
            if (type != "notReleased" && type != "wanted" && type != "owned")
            {
                Error = true;
                ErrorMessage = "Please use one of the buttons associated with the series you want to add a book to.";
                return Page();
            }

            AddBooks.BookType = type;
            AddBooks.Series = SessionController.SeriesManager.Series[seriesNr];
            AddBooks.FromSeries = true;
            
            return RedirectToPage("AddBooks");
        }

        /**
         * Modify information for given series
         */
        public void OnGetChangeSeries(int seriesId, bool complete, bool boring, bool read, int rating)
        {
            try
            {
                SessionController.SeriesManager.UpdateSeriesInfo(seriesId, complete, boring, read, rating);
                Success = true;
                SuccessMessage = $"Updated {SessionController.SeriesManager.Series[seriesId]}";
            }
            catch (Exception e)
            {
                Success = false;
                Error = true;
                ErrorMessage = e.Message;
            }
            finally
            {
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }
            }
            
        }

        /**
         * Set series to modify so form can be loaded
         */
        public void OnPostChangeSeries(int seriesId)
        {
            SeriesToModify = SessionController.SeriesManager.Series[seriesId];
        }

        // set info for add series form
        public void OnPostAddSeries(int seriesId)
        {
            AuthorNewSeries = SessionController.SeriesManager.Series[seriesId].Author;
        }
        
        // add series to program/database
        public void OnGetAddSeries(string authorFirst, string authorInitials, 
            string authorLast, string seriesName, bool complete,
            bool tooBoring, bool read, int rating)
        {
            if (string.IsNullOrEmpty(authorFirst) && string.IsNullOrEmpty(authorLast))
            {
                Error = true;
                Success = false;
                ErrorMessage = "provide at least one of first and last name for the author.";
                return;
            }
            try
            {
                var seriesManager = SessionController.SeriesManager;
                var author = AuthorNewSeries ?? new Author(authorFirst, authorInitials, authorLast);
                var series = seriesManager.AddNewSeries(author, seriesName, complete, tooBoring, read, rating);

                Success = true;
                SuccessMessage = $"Added {series}";
            }
            catch (Exception e)
            {
                Success = false;
                Error = true;
                ErrorMessage = e.Message;
            }
            finally
            {
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }

                AuthorNewSeries = null;
            }
        }
    }
}