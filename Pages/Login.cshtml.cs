﻿using System.Collections.Generic;
using libraryApi.Data;
using libraryApi.Models;
using libraryApi.Models.Exceptions;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace libraryApi.Pages
{
    public class Login : PageModel
    {
        // Properties
        public bool Error { get; set; }
        public string ErrorMessage { get; set; }
        
        // Header info
        // Information for page styling
        public Dictionary<string, string> HeaderItems { get; } =
            new Dictionary<string, string>(2)
            {
                {"active", null},
                {"header", "Database login"}
            };

        public List<List<string>> Scripts { get; } = new List<List<string>>();

        // Handler methods
        public void OnPostDbLogin(string server, string database, string username, string password, bool remember)
        {
            Error = false;
            var creds = new Dictionary<string, string>();
            creds["server"] = server;
            creds["database"] = database;
            creds["username"] = username;
            creds["password"] = password;
            SessionController.DatabaseCreds = creds;
            SessionController.RemoveCreds = !remember;
            try
            {
                ConnectionManager.DefaultManager();
            }
            catch (DatabaseException e)
            {
                Error = true;
                ErrorMessage = e.Message;
                SessionController.DatabaseCreds = null;
            }
        }

        public void OnGet()
        {
            
        }
    }
}