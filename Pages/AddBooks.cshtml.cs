﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Diagnostics;
using libraryApi.Models;
using libraryApi.Models.BookManagement;
using libraryApi.Models.Books;
using libraryApi.Models.SeriesManagement;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace libraryApi.Pages
{
    public class AddBooks : PageModel
    {
        // Properties
        public bool Error { get; set; }
        public string? ErrorMessage { get; set; }
        public bool Success { get; set; }
        public string? SuccessMessage { get; set; }
        
        // pre-defined parts for form
        public static Series? Series { get; set; }
        public static string? BookType { get; set; }
        public static Author? Author { get; set; }
        public static bool FromSeries { get; set; }
        public static bool FromBooks { get; set; }
        
        // Information for page styling
        public Dictionary<string, string> HeaderItems { get; } =
            new Dictionary<string, string>(2)
            {
                {"active", "Add Books"},
                {"header", "Add books to database"}
            };

        public List<List<string>> Scripts { get; } = new List<List<string>>
        {
            new List<string>()
            {
                "/js/manage_add_form.js",
                "text/javascript"
            }
        };
        
        // Page handlers
        public void OnGet()
        {
            if (!FromSeries)
            {
                Series = null;
                BookType = null;
            }

            if (!FromBooks)
            {
                Author = null;
            }

            FromSeries = false;
            FromBooks = false;
        }

        public void OnGetAdd(string bookType, 
            string numberInSeries, bool completesSeries, string authorFirst, string authorLast,
            string authorInitials, string title, string language, 
            string releaseDate, bool stillAgenda, bool thirdPartyBook,
            bool read, bool tooBoring, bool stillCalibre, bool stillEreader, 
            bool stillBackup, string type, bool novel68, bool audioAvailable, bool audioOnly)
        {
            Error = false;
            Success = false;
            Author? author = null;
            try
            {
                if (string.IsNullOrEmpty(authorFirst) && string.IsNullOrEmpty(authorLast))
                {
                    Error = true;
                    ErrorMessage = "Author firstname or author lastname has to be provided at the least.";
                    return;
                }

                // get author information
                if (Series != null)
                {
                    author = Series.Author;
                }
                else
                {
                    author = Author ?? new Author(authorFirst, authorInitials, authorLast);
                }

                var seriesNr = 0;
                if (Series != null)
                {
                    seriesNr = Series.SeriesNr;
                }
                
                // create book object
                Book? book = null;
                switch (bookType)
                {
                    case "notReleased":
                        book = new NotReleasedBook(author, seriesNr, numberInSeries, title, language, releaseDate,
                            stillAgenda, 0);
                        book.AddToDatabase();
                        var notReleasedBook = (NotReleasedBook) book;
                        BooksManager.NotReleasedBooks[notReleasedBook.TableId] = notReleasedBook;
                        break;
                    case "wanted":
                        book = new WantedBook(author, seriesNr, numberInSeries, title, language, 0);
                        book.AddToDatabase();
                        var wantedBook = (WantedBook) book;
                        BooksManager.WantedBooks[wantedBook.TableId] = wantedBook;
                        break;
                    case "owned":
                        book = new OwnedBook(
                            author, seriesNr, numberInSeries, title,
                            language, 0, stillCalibre, stillEreader,
                            stillBackup, novel68, thirdPartyBook, read,
                            tooBoring, type, audioAvailable,
                            audioOnly, completesSeries);
                        book.AddToDatabase();
                        var ownedBook = (OwnedBook) book;
                        BooksManager.OwnedBooks[ownedBook.BookNr] = ownedBook;
                        break;
                }

                if (book != null)
                {
                    Success = true;
                    SuccessMessage = $"Successfully added book {book} to database.";   
                }
                else
                {
                    Error = true;
                    ErrorMessage = "invalid book type given, select one in the dropdown in the addbooks form.";
                }
            }
            catch (Exception e)
            {
                Error = true;
                ErrorMessage = e.Message;
                // a new author was made that now won't be used, so needs to be deleted from database.
                if (author != null && Author == null && Series == null)
                {
                    author.RemoveFromDatabase();
                }
            }
            finally
            {
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }
                Series = null;
                BookType = null;
                Author = null;
            }
            
        }
    }
}