﻿#nullable enable
using System;
using System.Collections.Generic;
using libraryApi.Models;
using libraryApi.Models.BookManagement;
using libraryApi.Models.Books;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace libraryApi.Pages
{
    public class Owned : PageModel
    {
        // Properties
        public bool Error { get; set; }
        public string? ErrorMessage { get; set; }
        public bool Success { get; set; }
        public string? SuccessMessage { get; set; }
        public OwnedBook? BookToModify { get; set; }
        
        // Information for page styling
        public Dictionary<string, string> HeaderItems { get; } =
            new Dictionary<string, string>(2)
            {
                {"active", "Owned Books"},
                {"header", "Owned books management"}
            };

        public List<List<string>> Scripts { get; } = new List<List<string>>
        {
            new List<string>
            {
                "/js/manageDatatableOwned.js",
                "text/javascript"
            }
        };
        
        // Page handlers
        public void OnGet()
        {
            
        }
        
        /**
         * Modify information for given book
         */
        public void OnGetChangeBook(int bookId, bool boring, bool read, 
            bool calibre, bool ereader, bool backup, bool audioAvailable,
            bool audioOnly)
        {
            var book = BooksManager.OwnedBooks[bookId];
            try
            {
                SessionController.OwnedManager.ChangeBook(bookId, boring, 
                    read, calibre, ereader, backup, audioAvailable, audioOnly);
                Success = true;
                SuccessMessage = $"Successfully updated {book}";
            }
            catch (Exception e)
            {
                Success = false;
                Error = true;
                ErrorMessage = e.Message;
            }
            finally
            {
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }
            }
            
        }

        /**
         * Set book to modify so form can be loaded
         */
        public void OnPostChangeBook(int bookId)
        {
            BookToModify = BooksManager.OwnedBooks[bookId];
        }
        
        public IActionResult OnPostAdd(int bookId)
        {
            AddBooks.Author = BooksManager.OwnedBooks[bookId].Author;
            AddBooks.FromBooks = true;
            
            return RedirectToPage("AddBooks");
        }
    }
}