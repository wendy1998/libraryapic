﻿#nullable enable
using System;
using System.Collections.Generic;
using libraryApi.Models;
using libraryApi.Models.BookManagement;
using libraryApi.Models.SeriesManagement;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace libraryApi.Pages
{
    public class SeriesPicker : PageModel
    {
        // Information for page styling
        public Dictionary<string, string> HeaderItems { get; } =
            new Dictionary<string, string>(2)
            {
                {"active", "Series Generator"},
                {"header", "Generate random series"}
            };
        
        public static Dictionary<int, Series> PickedSeries { get; set; } = new Dictionary<int, Series>();
        public bool Error { get; set; }
        public string? ErrorMessage { get; set; }
        public bool Success { get; set; }
        public List<List<string>> Scripts { get; } = new List<List<string>>();
        public string? SuccessMessage { get; set; } = null;

        // Method handlers
        public void OnGet()
        {

        }

        public void OnGetChangeRating(int seriesId, bool increase) {
            try
            {
                PickedSeries[seriesId].ChangeRating(increase);
                Success = true;
                SuccessMessage =
                    $"Successfully {(increase ? "increased" : "decreased")} rating of {PickedSeries[seriesId]}";
            }
            catch
            {
                Error = true;
                Success = false;
            }
            finally
            {
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }
            }
        }

        public void OnGetRandom(int amount)
        {
            PickedSeries = new Dictionary<int, Series>(amount);
            SessionController.SeriesManager ??= new SeriesManager();
            List<Series> toPickFrom = new List<Series>();
            foreach (var series in SessionController.SeriesManager.Series.Values)
            {
                if (!series.Complete || series.TooBoring || series.Read) continue;
                for (var i = 0; i < series.Rating; i++)
                {
                    toPickFrom.Add(series);
                }
            }

            try
            {
                var random = new Random();
                for (var i = 0; i < amount; i++)
                {
                    while (PickedSeries.Count < i + 1)
                    {
                        var picked = toPickFrom[random.Next(toPickFrom.Count)];
                        if (!PickedSeries.ContainsKey(picked.SeriesNr))
                        {
                            PickedSeries[picked.SeriesNr] = picked;
                        }
                    }
                }

                Success = true;
                SuccessMessage = $"Successfully retrieved {amount.ToString()} random series.";
            }
            catch (Exception e)
            {
                Success = false;
                Error = true;
                ErrorMessage = e.Message;
            }
            finally
            {
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }
            }
        }
    }
}