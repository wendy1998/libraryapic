﻿#nullable enable
using System;
using System.Collections;
using System.Collections.Generic;
using libraryApi.Data;
using libraryApi.Models;
using libraryApi.Models.Agenda;
using libraryApi.Models.BookManagement;
using libraryApi.Models.Books;
using libraryApi.Models.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace libraryApi.Pages
{
    public class IndexModel : PageModel
    {
        public bool Success { get; set; }
        public string? SuccessMessage { get; private set; }
        public bool Error { get; set; }
        public string? ErrorMessage { get; set; }

        // Information for page styling
        public Dictionary<string, string> HeaderItems { get; } =
            new Dictionary<string, string>(2)
            {
                {"active", "Home"},
                {"header", "Task request"}
            };

        public List<List<string>> Scripts { get; } = new List<List<string>>
        {
            new List<string>
            {
                "/js/manageDatatablesHome.js",
                "text/javascript"
            }
        };

        public static List<Dictionary<string, object>>? StillAgenda { get; set; }
        public static List<Dictionary<string, object>>? StillCalibre { get; set; }
        public static List<Dictionary<string, object>>? StillEreader { get; set; }
        public static List<Dictionary<string, object>>? StillBackup { get; set; }

        // Handler methods
        public void OnGet()
        {
            if (SessionController.DatabaseCreds == null) return;
            StillAgenda = ViewManager.GetStillAgenda();
            StillCalibre = ViewManager.GetStillCalibre();
            StillEreader = ViewManager.GetStillEreader();
            StillBackup = ViewManager.GetStillBackup();

        }

        public void OnGetOpenCalibre()
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C calibre";
            process.StartInfo = startInfo;
            process.Start();
        }

        public void OnGetUpdateCalibre()
        {
            Success = false;
            SuccessMessage = null;
            Error = false;
            ErrorMessage = null;

            try
            {
                // update database
                var query = "update mijn_boeken set NOG_CALIBRE = '0' where NOG_CALIBRE = '1'";
                var manager = ConnectionManager.DefaultManager();
                manager.Update(query);

                // update object
                SessionController.OwnedManager ??= new OwnedManager();
                foreach (var bookInfo in StillCalibre)
                {
                    var id = int.Parse(bookInfo["BOEKNR"].ToString());
                    var book = BooksManager.OwnedBooks[id];
                    book.StillCalibre = false;
                }

                Success = true;
                SuccessMessage = "Successfully marked all books as not needing calibre.";
            }
            catch (Exception e)
            {
                Error = true;
                ErrorMessage = e.Message;
            }
            finally
            {
                StillCalibre = ViewManager.GetStillCalibre();
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }
            }
        }

        public void OnGetUpdateBackup()
        {
            Success = false;
            SuccessMessage = null;
            Error = false;
            ErrorMessage = null;

            try
            {
                // update database
                var query = "update mijn_boeken set NOG_BACKUP = '0' where NOG_BACKUP = '1'";
                var manager = ConnectionManager.DefaultManager();
                manager.Update(query);

                // update object
                SessionController.OwnedManager ??= new OwnedManager();
                foreach (var bookInfo in StillBackup)
                {
                    var id = int.Parse(bookInfo["BOEKNR"].ToString());
                    var book = BooksManager.OwnedBooks[id];
                    book.StillBackup = false;
                }

                Success = true;
                SuccessMessage = "Successfully marked all books as not needing backup.";
            }
            catch (Exception e)
            {
                Error = true;
                ErrorMessage = e.Message;
            }
            finally
            {
                StillBackup = ViewManager.GetStillBackup();
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }
            }
        }

        public void OnGetUpdateEreader()
        {
            Success = false;
            SuccessMessage = null;
            Error = false;
            ErrorMessage = null;

            try
            {
                // update database
                var query = "update mijn_boeken set NOG_EREADER = '0' where NOG_EREADER = '1'";
                var manager = ConnectionManager.DefaultManager();
                manager.Update(query);

                // update object
                SessionController.OwnedManager ??= new OwnedManager();
                foreach (var bookInfo in StillEreader)
                {
                    var id = int.Parse(bookInfo["BOEKNR"].ToString());
                    var book = BooksManager.OwnedBooks[id];
                    book.StillEReader = false;
                }

                Success = true;
                SuccessMessage = "Successfully marked all books as not needing to be added to the ereader.";
            }
            catch (Exception e)
            {
                Error = true;
                ErrorMessage = e.Message;
            }
            finally
            {
                StillEreader = ViewManager.GetStillEreader();
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }
            }
        }

        public void OnGetAgenda()
        {
            Success = false;
            SuccessMessage = null;
            Error = false;
            ErrorMessage = null;
            
            var manager = new AgendaManager();
            // gather books needed
            foreach (var bookInfo in StillAgenda)
            {
                var id = int.Parse(bookInfo["ID"].ToString());
                var book = BooksManager.NotReleasedBooks[id];
                try
                {
                    manager.AddToAgenda(book);
                    Success = true;
                    SuccessMessage = SuccessMessage == null
                        ? $"Successfully added {book}"
                        : $"{SuccessMessage}\nand {book}";
                }
                catch (Exception e)
                {
                    Error = true;
                    if (ErrorMessage != null)
                    {
                        ErrorMessage = $"{ErrorMessage}\nSomething went wrong while adding book {book}: {e.Message}.";
                    }
                    else
                    {
                        ErrorMessage = $"Something went wrong while adding book {book}: {e.Message}";
                    }
                }
            }

            StillAgenda = ViewManager.GetStillAgenda();
            if (SessionController.RemoveCreds)
            {
                SessionController.DatabaseCreds = null;
            }

            if (Success)
            {
                SuccessMessage = $"{SuccessMessage}\nTo agenda.";
            }
        }

        /**
         * Used to convert all the released books to wanted books.
         */
        public void OnGetConvertReleased()
        {
            try
            {
                Success = false;
                SuccessMessage = null;
                Error = false;
                SessionController.NotReleasedManager.UpdateReleasedBooks();
                var conversions = SessionController.NotReleasedManager.Logger.Conversions["not_released"];
                foreach (var (original, converted) in conversions)
                {
                    if (converted == null)
                    {
                        Error = true;
                    }
                    else
                    {
                        Success = true;
                        SuccessMessage = SuccessMessage == null
                            ? $"Successfully converted {original}"
                            : $"{SuccessMessage}\nand {original}";
                    }
                }

                if (Success)
                {
                    SuccessMessage = $"{SuccessMessage}\nto wanted book(s)";
                }
            }
            catch (Exception e)
            {
                Error = true;
                Success = false;
                ErrorMessage = e.Message;
            }
            finally
            {
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }
            }
        }
    }
}
