﻿#nullable enable
using System;
using System.Collections.Generic;
using libraryApi.Models;
using libraryApi.Models.Agenda;
using libraryApi.Models.BookManagement;
using libraryApi.Models.Books;
using libraryApi.Models.Searching;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace libraryApi.Pages
{
    public class NotReleased : PageModel
    {
        // Properties
        public bool Success { get; private set; }
        public string? SuccessMessage { get; private set; }
        public bool Error { get; private set; }
        public string? ErrorMessage { get; private set; }
        public NotReleasedBook? BookToModify { get; private set; }
        public static List<SearchResult?>? SearchResults { get; set; } = null;
        [BindProperty(SupportsGet = true)]
        public CustomResult CustomRes { get; set; } = new CustomResult();

        // Information for page styling
        public Dictionary<string, string> HeaderItems { get; } =
            new Dictionary<string, string>(2)
            {
                {"active", "Not Released"},
                {"header", "Search results for not released books"}
            };

        public List<List<string>> Scripts { get; } = new List<List<string>>()
        {
            new List<string>()
            {
                "/js/manageDatatableNotReleased.js",
                "text/javascript"
            },
            new List<string>()
            {
                "/js/manage_results_forms.js",
                "text/javascript"
            }
        };
        
        public class CustomResult
        {
            public int TitleIdx { get; set; }
            public int ReleaseIdx { get; set; }
            public string Input { get; set; }

            public Dictionary<string, string> ExtractInfo()
            {
                var output = new Dictionary<string, string>(2);
                var splt = Input.Split(",");
                if (TitleIdx > 0)
                {
                    output["TITEL"] = splt[TitleIdx - 1];
                }
                if (ReleaseIdx > 0)
                {
                    output["DATUM_UITKOMST"] = splt[ReleaseIdx - 1];
                }

                return output;
            }
        }
        
        // Page handlers
        public void OnGet()
        {
            
        }

        /**
         * Set book to modify so form can be loaded
         */
        public void OnPostChangeBook(int bookId)
        {
            Success = false;
            Error = false;
            BookToModify = BooksManager.NotReleasedBooks[bookId];
            var searchManager = new SearchManager();
            try
            {
                SearchResults = searchManager.PerformSearch(BookToModify);
            }
            catch (Exception e)
            {
                Error = true;
                ErrorMessage = e.Message;
                BookToModify = null;
            }
            finally
            {
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }
            }
        }
        
        /**
         * Set information for add book page and redirect there to add a new book.
         */
        public IActionResult OnPostAdd(int bookId)
        {
            AddBooks.Author = BooksManager.NotReleasedBooks[bookId].Author;
            AddBooks.FromBooks = true;
            
            return RedirectToPage("AddBooks");
        }
        
        /**
         * Modify information for given book
         */
        public void OnPostUpdate(string wayOfUpdate, string customResult, 
            string searchResult, string[] task, bool agenda, int bookId)
        {
            try
            {
                var notReleasedManager = SessionController.NotReleasedManager;
                var book = BooksManager.NotReleasedBooks[bookId];
                if (wayOfUpdate == "result")
                {
                    var result = searchResult;
                    var searchRes = result == "full" ? SearchResults?[1] : SearchResults?[0];

                    Success = notReleasedManager.UpdateRecord(book, searchRes, task);
                    SuccessMessage = $"Update of book {book} successful.";
                    Error = !Success;
                    ErrorMessage = $"Update of book {book} failed.";
                }
                else
                {
                    if (!string.IsNullOrEmpty(customResult))
                    {
                        CustomRes.Input = customResult;
                        var updateInfo = CustomRes.ExtractInfo();
                        Success = notReleasedManager.UpdateRecord(book, updateInfo);
                        SuccessMessage = $"Update of book {book} successful.";
                        Error = !Success;
                        ErrorMessage = $"Update of book {book} failed.";
                    }
                    else
                    {
                        Error = true;
                        ErrorMessage = "Nothing in custom result string, so nothing to update.";
                        Success = false;
                    }
                }

                if (Error)
                {
                    SessionController.NotReleasedManager.Logger.WriteErrorsToFile($"{Program.ContentRoot}/wwwroot/Logfile.txt");
                    return;
                }

                book.StillAgenda = agenda;
                book.UpdateDatabase(true);
            }
            catch
            {
                Error = true;
                Success = false;
            }
            finally
            {
                if (SessionController.RemoveCreds)
                {
                    SessionController.DatabaseCreds = null;
                }

                SearchResults = null;
            }
        }

        // Other methods
        public static HtmlString ConstructReleaseDate(SearchResult res)
        {
            var output = "";
            if (res.PublicationYear == 0) return new HtmlString(output);
            output = $"Release date:<br/>year: {res.PublicationYear.ToString()}";
            if (res.PublicationMonth == 0) return new HtmlString(output);
            output = $"{output}<br/>month: {res.PublicationMonth.ToString()}";
            if (res.PublicationDay == 0) return new HtmlString(output);
            output = $"{output}<br/>day: {res.PublicationDay.ToString()}";

            return new HtmlString(output);
        }
    }
}