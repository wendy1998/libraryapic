using System.Collections.Generic;
using System.Data;
using System.Linq;
using libraryApi.Models;
using libraryApi.Models.Exceptions;
using MySql.Data.MySqlClient;

namespace libraryApi.Data
{
    /**
     * Class that manages database connectivity.
     */
    internal class ConnectionManager
    {
        // Fields
        private static ConnectionManager _singletonInstance;
        private MySqlConnection _connection;
        private string _server;
        private string _database;
        private string _uid;
        private string _password;

        // Private constructor so singleton pattern can be used    
        private ConnectionManager()
        {
            Initialize();
        }
        
        /**
	     * Make sure only one instance of this class exists in this program.
	     * @return The single instance of this class
         * @throws DatabaseException: If no database credentials were passed to the session of the program.
	     */
        public static ConnectionManager DefaultManager()
        {
            if (SessionController.DatabaseCreds == null)
            {
                throw new DatabaseException("ConnectionManager: DefaultManager - Not logged into database");
            }
            return _singletonInstance ??= new ConnectionManager();
        }

        /**
         * Used to initialize database connection and thereby check the credentials passed.
         * @throws DatabaseException if credentials are wrong or something else goes wrong while connecting.
         */
        private void Initialize()
        {
            var creds = SessionController.DatabaseCreds;
            _server = creds["server"];
            _database = creds["database"];
            _uid = creds["username"];
            _password = creds["password"];
            var connectionString = $"SERVER={_server}; DATABASE={_database}; UID={_uid}; PASSWORD={_password};";

            _connection = new MySqlConnection(connectionString);
            try
            {
                OpenConnection();
            }
            finally
            {
                CloseConnection();
            }
        }


        /**
	     * Create database connection.
	     * @throws DatabaseException, when connection cannot be made.
	     */
        private void OpenConnection()
        {
            try
            {
                _connection.Open();
            }
            catch (MySqlException ex)
            {
                throw new DatabaseException($"DBConnect: OpenConnection - SQL Exception: {ex.Message}");
            }
        }

        /**
	     * close database connection.
	     * @throws DatabaseException, when connection cannot be closed.
	     */
        private void CloseConnection()
        {
            try
            {
                _connection.Close();
            }
            catch (MySqlException ex)
            {
                throw new DatabaseException($"DBConnect: CloseConnection - SQL Exception: {ex.Message}");
            }
        }
        
        // query database
        public void Delete(string query)
        {
            //open connection
            try
            {
                // Open connection
                OpenConnection();
                
                // create mysql command
                var cmd = new MySqlCommand
                {
                    CommandText = query,
                    Connection = _connection
                };

                //Execute query
                cmd.ExecuteNonQuery();

            }
            catch (MySqlException e)
            {
                throw new DatabaseException($"ConnectionManager: Delete - SQLError: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }
        
        /**
         * Used to insert something into the database
         * @param sql: sql statement to execute
         * @throws DatabaseException, if anything goes wrong.
         */
        public void Insert(string sql)
        {
            
            //open connection
            try
            {
                // Open connection
                OpenConnection();
                
                // create mysql command
                var cmd = new MySqlCommand
                {
                    CommandText = sql,
                    Connection = _connection
                };

                //Execute query
                cmd.ExecuteNonQuery();

            }
            catch (MySqlException e)
            {
                throw new DatabaseException($"ConnectionManager: Insert - SQLError: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }

        /**
         * Used to execute update statements into the database
         * @param query: the query to execute
         * @throws DatabaseException, if anything goes wrong.
         */
        public void Update(string query)
        {
            try
            {
                // Open connection
                OpenConnection();
                
                // create mysql command
                var cmd = new MySqlCommand
                {
                    CommandText = query,
                    Connection = _connection
                };

                //Execute query
                cmd.ExecuteNonQuery();

            }
            catch (MySqlException e)
            {
                throw new DatabaseException($"ConnectionManager: Update - SQLError: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }

        /**
         * Used to call stored procedures in the database, possibly with arguments
         * @param stmtName: the name of the stored procedure in the database
         * @param args: list with mysql arguments that need to be passed to the stored procedure.
         * @throws DatabaseException, if anything goes wrong.
         */
        public void CallStmt(string stmtName, List<MySqlParameter> args = null)
        {

            try
            {
                // Open connection (throws error if couldn't be opened)
                OpenConnection();
                
                // Create Command
                var cmd = new MySqlCommand(stmtName, _connection) {CommandType = CommandType.StoredProcedure};

                // Add possible arguments
                if (args != null)
                {
                    foreach (var parameter in args)
                    {
                        cmd.Parameters.Add(parameter);
                    }
                }
            
                // Execute command
                cmd.ExecuteNonQuery();
            } 
            catch (MySqlException e)
            {
                throw new DatabaseException($"ConnectionManager: CallStmt - SQLError: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }

        /**
         * Used to execute select queries against the database.
         * @param query: the sql query to execute
         * @return dictionary with the column names as keys and
         * lists with objects representing the returned values as values.
         * @throws DatabaseException, if anything goes wrong.
         */
        public Dictionary<string, List<object>> Select(string query)
        {
            // Create a dict to store the result
            var output = new Dictionary<string, List<object>>();
            MySqlDataReader dataReader = null;
            try
            {
                // Open connection
                OpenConnection();
            
                // Create Command
                var cmd = new MySqlCommand(query, _connection);
                
                // Create a data reader and Execute the command
                dataReader = cmd.ExecuteReader();
                
                // examine the columns returned in the dataset
                var columns = GetColumnProperties(dataReader);
                
                foreach (var column in columns)
                {   
                    // output[column_name] = a list with objects representing the values returned by the query.
                    output[column] = new List<object>();
                }
                
                // Read the data and store them in the dictionary
                while (dataReader.Read())
                {
                    for (var i = 0; i < dataReader.FieldCount; i++)
                    {
                        output[columns[i]].Add(dataReader.GetValue(i));
                    }
                }
                
            } 
            catch (MySqlException e)
            {
                throw new DatabaseException($"ConnectionManager: Select - SQLError: {e.Message}");
            }
            finally
            {
                CloseConnection();
                // close Data Reader if needed
                dataReader?.Close();   
            }
            
            return output;
        }
        
        /**
         * Used to read out column properties returned in a MysqlReader
         * @param reader: mysql reader that has columns stored in it.
         * @return list of column names returned
         */
        private static List<string> GetColumnProperties(MySqlDataReader reader)
        {
            var schema = reader.GetColumnSchema();
            return schema.Select(column => column.ColumnName).ToList();
        }
        
    }
}
